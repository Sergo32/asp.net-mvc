﻿function ModalDialogV3_show(response) {
	var guid = uuidv4();
	var close = $("<div/>").addClass("modal-modified-close cancel").attr("data-id", guid).append($("<i/>").addClass("fa fa-times"));
	var content = $("<div/>").addClass("content-modified");
	var dialog = $("<div/>").addClass("modal-dialog-modified animated").append(close).append(content);
	var newElem = $("<div/>").addClass("modal-modified").attr("id", guid).attr("role", "dialog").attr("data-backdrop", "static").append(dialog);
	$("body").append(newElem);

	$('#' + guid + ' .content-modified').html(response);
	$('#' + guid + ' .keep').attr("data-id", guid);
	$('#' + guid + ' .cancel').attr("data-id", guid);
	$('#' + guid + ' .modal-dialog-modified').removeClass('fadeOutRight').addClass('fadeInRight');
	$('#' + guid).modal('show');
}

function ModalDialogV3_hide(id) {
	$('#' + id + ' .modal-dialog-modified').removeClass('fadeInRight').addClass('fadeOutRight');
	setTimeout(function () {
		$('#' + id).modal('hide');
		$('#' + id).remove();
	}, 500);
}

$(document).on('click', '.cancel', function () {
	ModalDialogV3_hide($(this).attr("data-id"));
});

function uuidv4() {
	return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
		(c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
	);
}


//// Во внутренней разметке выпадающей шторки на кнопке "Отмена" нужно установить класс "cancel"
//// кнопке "Сохранить" нужно установить класс "keep", так можно будет получить id = $(this).attr("data-id") для закрытия нужной шторки
//// для эффекта плавного появления и скрытия нужно подключить animate.css
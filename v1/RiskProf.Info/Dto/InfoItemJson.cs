﻿using RiskProf.Auth.Client;
using RiskProf.Info.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RiskProf.Info.Dto
{
    public class InfoItemModel
    {
        public IEnumerable<InfoItemJSON> InfoItemList { get; set; }

        public InfoItemJSON InfoItemToEdit { get; set; }

        public IEnumerable<InfoItem_ItemCategory> InfoItem_ItemCategoryList { get; set; }

        public IEnumerable<InfoClaim> Claims { get; set; }

        public IEnumerable<InfoItemType> Types { get; set; }

        public IEnumerable<InfoItemCategory> InfoItemCategoryList { get; set; }

       // public IEnumerable<FileInfoEntity> Files { get; set; }

        public int? IdCategory { get; set; }
        public string CategoryToTitle { get; set; }
        public bool UserAvaliable { get; set; }
    }

    public class InfoItemJSON : InfoItemBase
    {
        public InfoItemJSON(Domain.InfoItem infoItem)
        {
            this.Id = infoItem.Id;
            this.IsActive = infoItem.IsActive;
            this.Title = infoItem.Title;
            this.Text = infoItem.Text;
            this.SourceTitle = infoItem.SourceTitle;
            this.SourceUrl = infoItem.SourceUrl;
            this.ItemCategoriesList = infoItem.ItemCategoriesList;
            this.DateDeleted = infoItem.DateDeleted.HasValue ? infoItem.DateDeleted.Value.ToString("dd.MM.yyyy HH:mm") : "";
            this.DateEnd = infoItem.DateEnd.HasValue ? infoItem.DateEnd.Value.ToString("dd.MM.yyyy HH:mm") : "";
            this.DatePublication = infoItem.DatePublication.HasValue ? infoItem.DatePublication.Value.ToString("dd.MM.yyyy HH:mm") : "";
            this.DateStart = infoItem.DateStart.HasValue ? infoItem.DateStart.Value.ToString("dd.MM.yyyy HH:mm") : "";
            this.Description = infoItem.Description;
            this.ExtraUrl = infoItem.ExtraUrl;
            this.ContentUrl = infoItem.ContentUrl;
            this.CategoryIds = infoItem.CategoryIds;
            this.ClaimBool = infoItem.ClaimBool;
            this.ClaimIds = infoItem.ClaimIds;
            this.ClaimsList = infoItem.ClaimsList;
            this.ClaimsListDemo = infoItem.ClaimsListDemo;
            this.Type = infoItem.Type;
            this.FilterRow = infoItem.FilterRow;
            this.TotalRow = infoItem.TotalRow;
            this.TransliteratedTitle = infoItem.TransliteratedTitle;
            this.TypeId = infoItem.TypeId;
            this.DemoText = infoItem.DemoText;
            this.PreviewURL = infoItem.PreviewURL;

        }
        public InfoItemJSON()
        {

        }


        public string AvailableOn { get; set; }
        // Открыт для 
        public bool IsOpened { get; set; }
        public bool IsDemo { get; set; }
        public bool IsClosed { get; set; }
        // Вывод в разные списки на вебинарах
        public bool IsFuture { get; set; }
        public bool IsPast { get; set; }
        public string DatePublication { get; set; }
        public string DateDeleted { get; set; }
        public string DateStart { get; set; }
        public string DateEnd { get; set; }
        public DateTime? SortDatePublication { get; set; }

    }
}
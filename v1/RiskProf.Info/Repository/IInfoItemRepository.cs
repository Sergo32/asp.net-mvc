﻿namespace RiskProf.Info.Repository
{
    using System;
    using System.Data;
    using System.Collections.Generic;

    using RiskProf.Info.Domain;
    using RiskProf.Info.Filter;

    public interface IInfoItemRepository
    {
        IEnumerable<InfoItemCategory> GetAllCategory(InfoItemFilter filter);
        IEnumerable<InfoItemCategory> GetHierarchyCategory(InfoItemFilter filter);
        IEnumerable<InfoItem> GetItems(InfoItemFilter filter);
        IEnumerable<InfoItemCategory> GetAllItemCategoryById(InfoItemFilter filter);
        IEnumerable<InfoItemClaim> GetAllClaimsForItem(InfoItemFilter filter);
        IEnumerable<InfoItemPage> GetAllPages(InfoItemFilter filter);
        IEnumerable<InfoItemType> GetAllType(InfoItemFilter filter);
        IEnumerable<InfoClaim> GetAllClaims(InfoItemFilter filter);
        int InsertItem(InfoItem item);
        void InsertItemCategory(DataTable dataItemCategory);

        void InsertCategory(InfoItemCategory itemCategory);
        void InsertItemClaim(DataTable dataItemClaim);
        void UpdateItem(InfoItem item);
        void UpdateItemActive(int? id, bool isActive);
        void UpdateItemIsDeleted(int? id, bool isDeleted, DateTime dateTime);
        void DeleteItemCategoryById(int? id);
        void DeleteItemClaimById(int? id);
        void DeleteCategory(int id);
        InfoItem GetItemById(int? Id);
        void UpdateCategory(InfoItemCategory itemCategory);
        bool CheckInfoCategoryLinkInfoItem(int categoryId);
        bool CheckInfoCategoryParent(int parentId);

    }
}

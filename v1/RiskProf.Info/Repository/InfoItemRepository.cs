﻿namespace RiskProf.Info.Repository
{
    using System;
    using System.Data;
    using System.Linq;
    using System.Collections.Generic;
    using RiskProf.Info.Domain;
    using RiskProf.Info.Filter;
    using RiskProf.Helpers;

    public class InfoItemRepository : IInfoItemRepository
    {
        private readonly string _connectionString;

        public InfoItemRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public List<KeyValuePair<string, object>> GetParamterFromFilter(InfoItemFilter filter)
        {
            var parameters = new List<KeyValuePair<string, object>>();
            if (filter == null) return parameters;
            if (filter.IsActive.HasValue) parameters.Add(new KeyValuePair<string, object>("@IsActive", filter.IsActive));
            if (filter.Id.HasValue) parameters.Add(new KeyValuePair<string, object>("@Id", filter.Id));
            if (filter.CategoryId.HasValue) parameters.Add(new KeyValuePair<string, object>("@CategoryId", filter.CategoryId));
            if (!string.IsNullOrEmpty(filter.Title)) parameters.Add(new KeyValuePair<string, object>("@Title", filter.Title));
            if (!string.IsNullOrEmpty(filter.DemoText)) parameters.Add(new KeyValuePair<string, object>("@DemoText", filter.DemoText));

            if (filter.DatePublicationToPastList.HasValue) parameters.Add(new KeyValuePair<string, object>("@DatePublicationToPastList", filter.DatePublicationToPastList));
            if (filter.DatePublicationToFutureList.HasValue) parameters.Add(new KeyValuePair<string, object>("@DatePublicationToFutureList", filter.DatePublicationToFutureList));
            if (filter.DatePublication.HasValue) parameters.Add(new KeyValuePair<string, object>("@DatePublication", filter.DatePublication));
            if (filter.DatePublicationMax.HasValue) parameters.Add(new KeyValuePair<string, object>("@DatePublicationMax", filter.DatePublicationMax));
            if (filter.DatePublicationMin.HasValue) parameters.Add(new KeyValuePair<string, object>("@DatePublicationMin", filter.DatePublicationMin));
            if (filter.DatePublicationMax.HasValue) parameters.Add(new KeyValuePair<string, object>("@DateStartPublicationMax", filter.DatePublicationMax));
            if (filter.DatePublicationMin.HasValue) parameters.Add(new KeyValuePair<string, object>("@DateStartPublicationMin", filter.DatePublicationMin));
            if (filter.DatePublicationMax.HasValue) parameters.Add(new KeyValuePair<string, object>("@DateEndPublicationMax", filter.DatePublicationMax));
            if (filter.DatePublicationMin.HasValue) parameters.Add(new KeyValuePair<string, object>("@DateEndPublicationMin", filter.DatePublicationMin));
            if (filter.UrlToPast != null) parameters.Add(new KeyValuePair<string, object>("@UrlToPast", filter.UrlToPast));
            if (filter.UrlToFuture != null) parameters.Add(new KeyValuePair<string, object>("@UrlToFuture", filter.UrlToFuture));
            if (!string.IsNullOrEmpty(filter.SortColumnName)) parameters.Add(new KeyValuePair<string, object>("@SortColumnName", filter.SortColumnName));
            if (!string.IsNullOrEmpty(filter.SortDirection)) parameters.Add(new KeyValuePair<string, object>("@SortDirection", filter.SortDirection));
            if (filter.Start.HasValue) parameters.Add(new KeyValuePair<string, object>("@Start", filter.Start));
            if (filter.Lenght.HasValue) parameters.Add(new KeyValuePair<string, object>("@Lenght", filter.Lenght));
            if (filter.ParentId.HasValue) parameters.Add(new KeyValuePair<string, object>("@ParentId", filter.ParentId));
            if (filter.TypeId.HasValue) parameters.Add(new KeyValuePair<string, object>("@TypeId", filter.TypeId));
            if (filter.PageId.HasValue) parameters.Add(new KeyValuePair<string, object>("@PageId", filter.PageId));
            if (filter.IsDemo.HasValue) parameters.Add(new KeyValuePair<string, object>("@IsDemo", filter.IsDemo));
            if (filter.Guid != null) parameters.Add(new KeyValuePair<string, object>("@Guid", filter.Guid));
            return parameters;
        }


        public bool CheckInfoCategoryLinkInfoItem(int categoryId)
        {
            return _connectionString.ExecuteScalar<bool>("[Info].[CheckInfoCategoryLinkInfoItem]", true, new List<KeyValuePair<string, object>>
            {
                 new KeyValuePair<string, object>("@CategoryId", categoryId)

            });
        }

        public bool CheckInfoCategoryParent(int parentId)
        {
            return _connectionString.ExecuteScalar<bool>("[Info].[CheckInfoCategoryParent]", true, new List<KeyValuePair<string, object>>
            {
                 new KeyValuePair<string, object>("@ParentId", parentId)

            });
        }



        #region Select


        public IEnumerable<InfoItemCategory> GetAllItemCategoryById(InfoItemFilter filter)
        {
            var actions = new List<Action<IDataRecord, IList<InfoItemCategory>>>();
            Action<IDataRecord, IList<InfoItemCategory>> convertProject = (record, list) =>
            {
                var entity = new InfoItemCategory
                {
                    CategoryId = record.Get<int>("CategoryId"),
                    TitleCategory = record.GetN<string>("Title"),
                    Comment = record.GetN<string>("Comment")

                };
                list.Add(entity);
            };
            actions.Add(convertProject);
            return _connectionString.ExecuteSelectMany<InfoItemCategory>("[Info].[GetAllItemCategoryById]", true, GetParamterFromFilter(filter), converters: actions.ToArray());
        }

        public IEnumerable<InfoItemClaim> GetAllClaimsForItem(InfoItemFilter filter)
        {
            var actions = new List<Action<IDataRecord, IList<InfoItemClaim>>>();
            Action<IDataRecord, IList<InfoItemClaim>> convertProject = (record, list) =>
            {
                var entity = new InfoItemClaim
                {
                    Id = record.Get<int>("Id"),
                    ItemId = record.Get<int>("ItemId"),
                    ClaimId = record.Get<int>("ClaimId"),
                    IsDemo = record.Get<bool>("IsDemo"),
                   // Guid = record.Get<Guid>("Guid"),
                    AuthKey = record.Get<string>("AuthKey")
                };
                list.Add(entity);
            };
            actions.Add(convertProject);
            return _connectionString.ExecuteSelectMany<InfoItemClaim>("[Info].[GetAllClaimsForItem]", true, GetParamterFromFilter(filter), converters: actions.ToArray());
        }

        public IEnumerable<InfoItemCategory> GetAllCategory(InfoItemFilter filter)
        {
            var actions = new List<Action<IDataRecord, IList<InfoItemCategory>>>();
            Action<IDataRecord, IList<InfoItemCategory>> convertProject = (record, list) =>
            {
                var entity = new InfoItemCategory
                {
                    CategoryId = record.Get<int>("Id"),
                    TitleCategory = record.GetN<string>("Title"),
                    Comment = record.GetN<string>("Comment"),
                    PageId = record.GetN<int>("PageId"),
                    ParentId = record.GetN<int>("ParentId"),
                    Sort = record.GetN<int>("Sort")
                };
                list.Add(entity);
            };
            actions.Add(convertProject);
            return _connectionString.ExecuteSelectMany<InfoItemCategory>("[Info].[GetAllCategory]", true, GetParamterFromFilter(filter), converters: actions.ToArray());
        }
        public IEnumerable<InfoItemCategory> GetHierarchyCategory(InfoItemFilter filter)
        {
            var actions = new List<Action<IDataRecord, IList<InfoItemCategory>>>();
            Action<IDataRecord, IList<InfoItemCategory>> convertProject = (record, list) =>
            {
                var entity = new InfoItemCategory
                {
                    CategoryId = record.Get<int>("Id"),
                    TitleCategory = record.Get<string>("Title"),
                    ParentId = record.GetN<int>("ParentId"),
                    Level = record.GetN<int>("Level"),
                    Comment = record.GetN<string>("Comment")

                };
                list.Add(entity);
            };
            actions.Add(convertProject);
            return _connectionString.ExecuteSelectMany<InfoItemCategory>("[Info].[GetHierarchyCategory]", true, GetParamterFromFilter(filter), converters: actions.ToArray());
        }

        public IEnumerable<InfoItemPage> GetAllPages(InfoItemFilter filter)
        {
            var actions = new List<Action<IDataRecord, IList<InfoItemPage>>>();
            Action<IDataRecord, IList<InfoItemPage>> convertProject = (record, list) =>
            {
                var entity = new InfoItemPage
                {
                    Id = record.Get<int>("Id"),
                    Guid = record.Get<Guid>("Guid"),
                    Title = record.Get<string>("Title")
                };
                list.Add(entity);
            };
            actions.Add(convertProject);
            return _connectionString.ExecuteSelectMany<InfoItemPage>("[Info].[GetAllPages]", true, GetParamterFromFilter(filter), converters: actions.ToArray());
        }

        public IEnumerable<InfoItemType> GetAllType(InfoItemFilter filter)
        {
            var actions = new List<Action<IDataRecord, IList<InfoItemType>>>();
            Action<IDataRecord, IList<InfoItemType>> convertProject = (record, list) =>
            {
                var entity = new InfoItemType
                {
                    Id = record.Get<int>("Id"),
                    Title = record.Get<string>("Title"),
                    Guid = record.Get<Guid>("Guid")
                };
                list.Add(entity);
            };
            actions.Add(convertProject);
            return _connectionString.ExecuteSelectMany<InfoItemType>("[Info].[GetAllType]", true, GetParamterFromFilter(filter), converters: actions.ToArray());
        }

        public IEnumerable<InfoClaim> GetAllClaims(InfoItemFilter filter)
        {
            var actions = new List<Action<IDataRecord, IList<InfoClaim>>>();
            Action<IDataRecord, IList<InfoClaim>> convertProject = (record, list) =>
            {
                var entity = new InfoClaim
                {
                    Id = record.Get<int>("Id"),
                    Title = record.Get<string>("Title"),
                    AuthKey = record.Get<string>("AuthKey")
                };
                list.Add(entity);
            };
            actions.Add(convertProject);
            return _connectionString.ExecuteSelectMany<InfoClaim>("[Info].[GetAllClaims]", true, GetParamterFromFilter(filter), converters: actions.ToArray());
        }

        public IEnumerable<InfoItem> GetItems(InfoItemFilter filter)
        {
            var actions = new List<Action<IDataRecord, IList<InfoItem>>>();
            Action<IDataRecord, IList<InfoItem>> notify = (record, list) =>
            {
                var entity = new InfoItem
                {
                    Id = record.Get<int>("Id"),
                    Title = record.Get<string>("Title"),
                    Text = record.Get<string>("Text"),
                    Description = record.Get<string>("Description"),
                    SourceTitle = record.Get<string>("SourceTitle"),
                    SourceUrl = record.Get<string>("SourceUrl"),
                    ContentUrl = record.Get<string>("ContentUrl"),
                    ExtraUrl = record.Get<string>("ExtraUrl"),
                    DatePublication = record.Get<DateTime>("DatePublication"),
                    IsDeleted = record.Get<bool>("IsDeleted"),
                    DateDeleted = record.GetN<DateTime?>("DateDeleted"),  //item.RootActionGuid.HasValue ? (object)item.RootActionGuid.Value : DBNull.Value;,
                    DateStart = record.GetN<DateTime?>("DateStart"),
                    DateEnd = record.GetN<DateTime?>("DateEnd"),
                    IsActive = record.Get<bool>("IsActive"),
                    TransliteratedTitle = record.Get<string>("TransliteratedTitle"),
                    TypeId = record.Get<int>("TypeId"),
                    DemoText = record.GetN<string>("DemoText"),
                    PreviewURL = record.Get<string>("PreviewURL")

                };
                list.Add(entity);
            };
            actions.Add(notify);
            Action<IDataRecord, IList<InfoItem>> rowCount = (record, list) =>
            {
                int totalRows = record.Get<int>("TotalRows");
                int filtredRows = record.Get<int>("FilteredRows");
                InfoItem validation = list.FirstOrDefault();
                if (validation != null)
                {
                    validation.TotalRow = totalRows;
                    validation.FilterRow = filtredRows;
                }
            };
            actions.Add(rowCount);
            return _connectionString.ExecuteSelectMany<InfoItem>("[Info].[GetItems]", true, GetParamterFromFilter(filter), converters: actions.ToArray());
        }

        public InfoItem GetItemById(int? Id)
        {
            return GetItems(new InfoItemFilter { Id = Id }).FirstOrDefault();
        }

        #endregion

        #region Update

        public void UpdateItemActive(int? id, bool isActive)
        {
            _connectionString.ExecuteNonQuery("[Info].[UpdateItemActive]", true, new List<KeyValuePair<string, object>>
            {
                 new KeyValuePair<string, object>("@Id", id),
                     new KeyValuePair<string, object>("@IsActive", isActive)
            });
        }
        public void UpdateItemIsDeleted(int? id, bool isDeleted, DateTime dateTime)
        {
            _connectionString.ExecuteNonQuery("[Info].[UpdateItemIsDeleted]", true, new List<KeyValuePair<string, object>>
            {
                new KeyValuePair<string, object>("@Id", id),
                new KeyValuePair<string, object>("@DateDeleted", dateTime),
                new KeyValuePair<string, object>("@IsDeleted", isDeleted)
            });
        }

        public void UpdateItem(InfoItem item)
        {
            _connectionString.ExecuteNonQuery("[Info].[UpdateItem]", true, new List<KeyValuePair<string, object>>
            {
                new KeyValuePair<string, object>("@Id", item.Id),
                new KeyValuePair<string, object>("@Title", item.Title),
                new KeyValuePair<string, object>("@Text", item.Text ?? string.Empty),
                new KeyValuePair<string, object>("@ContentUrl", item.ContentUrl ?? string.Empty),
                new KeyValuePair<string, object>("@ExtraUrl", item.ExtraUrl ?? string.Empty),
                new KeyValuePair<string, object>("@SourceTitle", item.SourceTitle ?? string.Empty),
                new KeyValuePair<string, object>("@SourceUrl", item.SourceUrl ?? string.Empty),
                new KeyValuePair<string, object>("@IsDeleted", item.IsDeleted),
                new KeyValuePair<string, object>("@Description", item.Description ?? string.Empty),
                new KeyValuePair<string, object>("@DatePublication", item.DatePublication),
                new KeyValuePair<string, object>("@DateDeleted", item.DateDeleted),
                new KeyValuePair<string, object>("@DateStart", item.DateStart),
                new KeyValuePair<string, object>("@DateEnd", item.DateEnd),
                new KeyValuePair<string, object>("@TransliteratedTitle", item.TransliteratedTitle),
                new KeyValuePair<string, object>("@TypeId", item.TypeId),
                new KeyValuePair<string, object>("@DemoText", item.DemoText ?? string.Empty),
                new KeyValuePair<string, object>("@PreviewURL", item.PreviewURL ?? string.Empty)

            });
        }

        public void UpdateCategory(InfoItemCategory itemCategory)
        {
            _connectionString.ExecuteNonQuery("[Info].[UpdateCategory]", true, new List<KeyValuePair<string, object>>
            {
                new KeyValuePair<string, object>("@Id", itemCategory.CategoryId),
                new KeyValuePair<string, object>("@Title", itemCategory.TitleCategory),
                new KeyValuePair<string, object>("@ParentId", itemCategory.ParentId),
                new KeyValuePair<string, object>("@PageId", itemCategory.PageId),
                new KeyValuePair<string, object>("@Sort", itemCategory.Sort),
                new KeyValuePair<string, object>("@Comment", itemCategory.Comment ?? string.Empty)
            });
        }
        #endregion

        #region Insert
        public int InsertItem(Domain.InfoItem item)
        {
            return _connectionString.ExecuteScalar<int>("[Info].[InsertItem]", true, new List<KeyValuePair<string, object>>
            {
                new KeyValuePair<string, object>("@Title", item.Title),
                new KeyValuePair<string, object>("@Text", item.Text ?? string.Empty),
                new KeyValuePair<string, object>("@ContentUrl", item.ContentUrl ?? string.Empty),
                new KeyValuePair<string, object>("@SourceTitle", item.SourceTitle ?? string.Empty),
                new KeyValuePair<string, object>("@SourceUrl", item.SourceUrl ?? string.Empty),
                new KeyValuePair<string, object>("@ExtraUrl", item.ExtraUrl ?? string.Empty),
                new KeyValuePair<string, object>("@IsDeleted", item.IsDeleted),
                new KeyValuePair<string, object>("@IsActive", item.IsActive),
                new KeyValuePair<string, object>("@Description", item.Description ?? string.Empty),
                new KeyValuePair<string, object>("@DatePublication", item.DatePublication),
                new KeyValuePair<string, object>("@DateDeleted", item.DateDeleted),
                new KeyValuePair<string, object>("@DateStart", item.DateStart),
                new KeyValuePair<string, object>("@DateEnd", item.DateEnd),
                new KeyValuePair<string, object>("@TransliteratedTitle", item.TransliteratedTitle),
                new KeyValuePair<string, object>("@TypeId", item.TypeId),
                new KeyValuePair<string, object>("@DemoText", item.DemoText ?? string.Empty),
                new KeyValuePair<string, object>("@PreviewURL", item.PreviewURL ?? string.Empty)

            });
        }


        public void InsertCategory(InfoItemCategory itemCategory)
        {
            _connectionString.ExecuteNonQuery("[Info].[InsertCategory]", true, new List<KeyValuePair<string, object>>
            {
                  new KeyValuePair<string, object>("@Title", itemCategory.TitleCategory),
                  new KeyValuePair<string, object>("@ParentId", itemCategory.ParentId),
                  new KeyValuePair<string, object>("@Sort", itemCategory.Sort),
                  new KeyValuePair<string, object>("@PageId", itemCategory.PageId),
                  new KeyValuePair<string, object>("@Comment", itemCategory.Comment ?? string.Empty)
            });
        }

        public void InsertItemCategory(DataTable dataItemCategory)
        {
            _connectionString.ExecuteNonQuery("[Info].[InsertItemCategory]", true, new List<KeyValuePair<string, object>>
            {
              new KeyValuePair<string, object>("@ids", dataItemCategory)
            });
        }

        public void InsertItemClaim(DataTable dataItemClaim)
        {

            _connectionString.ExecuteNonQuery("[Info].[InsertItemClaim]", true, new List<KeyValuePair<string, object>>
            {
                 new KeyValuePair<string, object>("@ids", dataItemClaim)
        });
        }

        #endregion

        #region Delete


        public void DeleteItemCategoryById(int? id)
        {
            _connectionString.ExecuteNonQuery("[Info].[DeleteItemCategoryById]", true, new List<KeyValuePair<string, object>>
            {
                 new KeyValuePair<string, object>("@Id", id),
            });
        }

        public void DeleteItemClaimById(int? id)
        {
            _connectionString.ExecuteNonQuery("[Info].[DeleteItemClaimById]", true, new List<KeyValuePair<string, object>>
            {
                 new KeyValuePair<string, object>("@Id", id),
            });
        }
        public void DeleteCategory(int id)
        {
            _connectionString.ExecuteNonQuery("[Info].[DeleteCategory]", true, new List<KeyValuePair<string, object>>
            {
                 new KeyValuePair<string, object>("@Id", id),
            });
        }





        #endregion

    }
}

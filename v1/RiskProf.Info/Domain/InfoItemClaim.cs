﻿namespace RiskProf.Info.Domain
{
    using System;

    public class InfoItemClaim
    {
        public int? Id { get; set; }
        public int? ItemId { get; set; }
        public int? ClaimId { get; set; }
        public bool? IsDemo { get; set; }
        //public Guid Guid { get; set; }
        public string AuthKey { get; set; }
    }
}


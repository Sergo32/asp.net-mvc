﻿namespace RiskProf.Info.Domain
{
    using System;

    public class InfoItemType
    {     
        public int Id { get; set; }
        public string Title { get; set; }
        public Guid Guid { get; set; }
    }
}
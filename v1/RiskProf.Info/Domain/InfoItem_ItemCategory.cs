﻿namespace RiskProf.Info.Domain
{
    public class InfoItem_ItemCategory
    {
        public int? Id { get; set; }
        public int? IdItem { get; set; }
        public int? IdItemCategory { get; set; }
        public string TitleCategory { get; set; }
    }
}


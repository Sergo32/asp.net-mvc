﻿namespace RiskProf.Info.Domain
{
    using System;

    public class InfoItemPage
    {     
        public int Id { get; set; }
        public Guid Guid { get; set; }
        public string Title { get; set; }
    }
}
﻿namespace RiskProf.Info.Domain
{
    using System;
    using System.Globalization;
    using System.Collections.Generic;
    using RiskProf.Auth.Client;
    using RiskProf.Info.Dto;

    public class InfoItemBase
    {
        public int? Id { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public string Description { get; set; }
        public string SourceTitle { get; set; }
        public string SourceUrl { get; set; }
        public string ContentUrl { get; set; }
        public string PreviewURL { get; set; }
        public string ExtraUrl { get; set; }
        public string DemoText { get; set; }
        public bool? IsActive { get; set; }

        public string TransliteratedTitle { get; set; }
        public int?[] CategoryIds { get; set; }
        public int?[] ClaimIds { get; set; }
        public int?[] ClaimBool { get; set; }
        public int TotalRow { get; set; }
        public int FilterRow { get; set; }
        public int TypeId { get; set; }
        public IEnumerable<InfoItemCategory> ItemCategoriesList { get; set; }
        public IEnumerable<InfoItemClaim> ClaimsList { get; set; }
        public IEnumerable<InfoItemClaim> ClaimsListDemo { get; set; }
        public Domain.InfoItemType Type { get; set; }
        public IAuthorizationService AuthorizationService { get; set; }
    }

    public class InfoItem : InfoItemBase
    {
        public InfoItem(InfoItemJSON infoItemJSON)
        {
            this.Id = infoItemJSON.Id;
            this.IsActive = infoItemJSON.IsActive;
            this.Title = infoItemJSON.Title;
            this.Text = infoItemJSON.Text;
            this.SourceTitle = infoItemJSON.SourceTitle;
            this.SourceUrl = infoItemJSON.SourceUrl;
            this.ItemCategoriesList = infoItemJSON.ItemCategoriesList;
            DateTime date;
            if (DateTime.TryParseExact(infoItemJSON.DatePublication, "dd.MM.yyyy HH:mm", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
            {
                this.DatePublication = date;
            }
            if (DateTime.TryParseExact(infoItemJSON.DateEnd, "dd.MM.yyyy HH:mm", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
            {
                this.DateEnd = date;
            }
            if (DateTime.TryParseExact(infoItemJSON.DateStart, "dd.MM.yyyy HH:mm", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
            {
                this.DateStart = date;
            }
            if (DateTime.TryParseExact(infoItemJSON.DateDeleted, "dd.MM.yyyy HH:mm", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
            {
                this.DateDeleted = date;
            }
            this.Description = infoItemJSON.Description;
            this.ExtraUrl = infoItemJSON.ExtraUrl;
            this.ContentUrl = infoItemJSON.ContentUrl;
            this.CategoryIds = infoItemJSON.CategoryIds;
            this.ClaimBool = infoItemJSON.ClaimBool;
            this.ClaimIds = infoItemJSON.ClaimIds;
            this.ClaimsList = infoItemJSON.ClaimsList;
            this.ClaimsListDemo = infoItemJSON.ClaimsListDemo;
            this.Type = infoItemJSON.Type;
            this.FilterRow = infoItemJSON.FilterRow;
            this.TotalRow = infoItemJSON.TotalRow;
            this.TransliteratedTitle = infoItemJSON.TransliteratedTitle;
            this.TypeId = infoItemJSON.TypeId;
            this.DemoText = infoItemJSON.DemoText;
            this.PreviewURL = infoItemJSON.PreviewURL;
        }

        public InfoItem()
        {

        }

        //public Guid Guid { get; set; }
        public bool IsDeleted { get; set; }
        public int TypeId { get; set; }
        public DateTime? DatePublication { get; set; }
        public DateTime? DateDeleted { get; set; }
        public DateTime? DateStart { get; set; }
        public DateTime? DateEnd { get; set; }
    }

    static public class InfoItemPages
    {
        public static Guid Guide = new Guid("aff26511-1353-4eee-ab06-c88df054d05b");
        public static Guid Features = new Guid("1f5f86bc-c438-4190-ae8e-a66dbda18a97");
        public static Guid Partners = new Guid("6268064d-abee-4d8c-badc-81154eb01ce8");
        public static Guid Updates = new Guid("ab509414-8d7c-4daf-b907-8719d8df152e");
        public static Guid News = new Guid("e75acfc8-45e9-4d08-983d-906547710108");
        public static Guid Events = new Guid("08883760-d58b-4abb-8cad-1976703096c2");
        public static Guid Articles = new Guid("77922283-4b27-40f8-9ff4-98867652827d");
        public static Guid Materials = new Guid("68d9b41f-759c-48fa-9a19-f64b18eb44d3");
        public static Guid Webinars = new Guid("f9244818-4475-40a8-b6b6-9d83b144f321");
        public static Guid MethodicalMaterials = new Guid("7dfa0e78-b186-472f-ad06-33ab76ca4181");
        public static Guid NewsReview = new Guid("83bee856-8a74-4298-84b2-b776edb09d56");
        public static Guid SaleDocuments = new Guid("4a989cb0-a2d9-43db-b36c-6f31cc566bed");
        public static Guid VideoInstructions = new Guid("0356ede4-db21-4038-89fe-16891c102d56");
    }

    static public class InfoItemTypes
    {
        public static Guid Video = new Guid("dd1f33b5-8420-4b5e-8a96-539770d4ecc6");
        public static Guid Article = new Guid("1a8faf74-32b4-49fb-88dd-cc9dc0acfd77");

    }

}

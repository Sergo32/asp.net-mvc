﻿namespace RiskProf.Info.Domain
{
    using System;

    public class InfoClaim
    {     
        public int Id { get; set; }
        public string Title { get; set; }
        public string AuthKey { get; set; }
    }
}
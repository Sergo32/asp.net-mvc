﻿namespace RiskProf.Info.Domain
{
    using System.Text;

    public class InfoItemCategory
    {
        public int? CategoryId { get; set; }
        public string TitleCategory { get; set; }
        public string Comment { get; set; }
        public string InnerName 
        { 
            get
            {
                var sb = new StringBuilder();
                if (string.IsNullOrEmpty(this.Comment))
                {
                    sb.Append(TitleCategory);
                } else
                {
                    sb.AppendFormat("{0} ({1})", this.TitleCategory, this.Comment);
                }
                return sb.ToString();
            }
            set
            {
                
            }
        }
        public int? PageId { get; set; }
        public int? ParentId { get; set; }
        public int? Sort { get; set; }
        public int? Level { get; set; }



    }
}

﻿namespace RiskProf.Info.Filter
{
    using System;
    using System.Linq;

    public interface IPageFilter
    {
        IQueryable<TEntity> ChangePage<TEntity>(IQueryable<TEntity> query);
    }
    public class PagingByPage : IPageFilter
    {

        public int? Start { get; set; }
        public int? Lenght { get; set; }
        public int FilterRow { get; set; }
        public int TotalRow { get; set; }
        public string SortColumnName { get; set; }
        public string SortDirection { get; set; }

        public string Tabs { get; set; }
        public string pageParameterId { get; set; }

        /// <summary>
        /// Номер страницы. Нумерация с 1-цы
        /// </summary>
        public int? PageNo { get; set; }
        public int? PageSize { get; set; }
        public int? TotalRows { get; set; }
        public int? CountPages { get; set; }

        /// <summary>
        /// Есть значения для разбиения на страницы
        /// </summary>
        public bool HasSize
        {
            get
            {
                return
                    this.PageNo.HasValue &&
                    (this.PageNo > 0) &&
                    this.PageSize.HasValue &&
                    (this.PageSize > 0);
            }
        }

        public string FormId
        {
            get
            {
                return _formId;
            }

            set
            {
                _formId = value;
            }
        }

        public IQueryable<TEntity> ChangePage<TEntity>(IQueryable<TEntity> query)
        {
            if (query == null) return null;

            if (!(this.PageNo.HasValue && this.PageSize.HasValue)) return query;

            return query.Skip((this.PageNo.Value - 1) * this.PageSize.Value).Take(this.PageSize.Value);
        }

        #region Fields ---------------------------------------

        /// <summary>
        /// Для передачи в частичное представление Id формы, которая владеет просмотром страниц
        /// </summary>
        private string _formId = "";

        #endregion
    }

    public class InfoItemFilter : PagingByPage
    {
        public int? Id { get; set; }
        public int? IdItem { get; set; }
        public int? CategoryId { get; set; }
        public string Title { get; set; }
        public string DemoText { get; set; }
        public DateTime? DatePublicationMax { get; set; }
        public DateTime? DatePublicationMin { get; set; }
        public DateTime? DateStartPublicationMax { get; set; }
        public DateTime? DateStartPublicationMin { get; set; }
        public DateTime? DateEndPublicationMax { get; set; }
        public DateTime? DateEndPublicationMin { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? DatePublicationToPastList { get; set; }
        public DateTime? DatePublicationToFutureList { get; set; }
        public DateTime? DatePublication { get; set; }
        public string UrlToPast { get; set; }
        public string UrlToFuture { get; set; }
        public int? ParentId { get; set; }
        public int? TypeId { get; set; }
        public int? PageId { get; set; }
        public bool? IsDemo { get; set; }
        public int? CountLengths { get; set; }//для отоброжения из всех  отсортированных элементов из категорий число записей

        public Guid? Guid { get; set; }

    }
}

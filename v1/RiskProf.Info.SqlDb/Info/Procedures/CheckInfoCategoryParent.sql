﻿CREATE PROCEDURE [Info].[CheckInfoCategoryParent]
	@ParentId int	
as
declare @check bit 
if	(EXISTS (select Id from [Info].[Category] where ParentId = @ParentId) ) set @check=1
else  set @check=0
select @check


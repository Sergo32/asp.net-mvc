﻿CREATE PROCEDURE [Info].[UpdateCategory]
	@Id Int ,
	@Title nvarchar(max) ,
	@Comment nvarchar(max) ,
	@ParentId Int= null ,
    @PageId Int= null ,
    @Sort Int = null 
as

UPDATE [Info].[Category]
SET Title=@Title , Comment=@Comment, Sort=@Sort ,PageId=@PageId,ParentId = @ParentId  WHERE Id = @Id

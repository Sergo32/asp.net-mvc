﻿CREATE PROCEDURE [Info].[UpdateItemIsDeleted]
    @Id int,
    @DateDeleted datetime2,
    @IsDeleted bit 
as
UPDATE [Info].[Item]  set   DateDeleted = @DateDeleted, IsDeleted=@IsDeleted  where [Item].Id=@Id

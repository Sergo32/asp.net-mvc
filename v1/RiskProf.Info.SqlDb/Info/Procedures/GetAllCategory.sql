﻿CREATE PROCEDURE [Info].[GetAllCategory]
	@Id int = null,
	@ParentId int = null,
	@Guid uniqueidentifier = null,
	@PageId int = null
as
select * From [Info].[Category]
where 
(@Id is null or  Id=@Id) and
(@PageId  is null or  PageId=@PageId) and
(@ParentId is null or  ParentId=@ParentId) and
(@Guid is null or PageId = (SELECT Id FROM [Info].[Page] WHERE Guid = @Guid))
 order by sort asc

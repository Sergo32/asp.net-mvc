﻿
create type TableItemCategory as table (ItemId int,CategoryId int);

go
CREATE PROCEDURE [Info].[InsertItemCategory]
 @ids TableItemCategory readonly

as
INSERT INTO [Info].[ItemCategory]
select ItemId,CategoryId from  @ids



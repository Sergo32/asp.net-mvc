﻿CREATE PROCEDURE [Info].[GetAllClaimsForItem]
  	@Id int = null,
    @IsDemo bit = false
    as
select itemfunc.Id,itemfunc.ItemId,itemfunc.ClaimId,func.AuthKey,itemfunc.IsDemo
From [Info].[ItemClaim]  as itemfunc join [Info].[Claims] as func on itemfunc.ClaimId = func.Id
where (@Id is null or  itemfunc.ItemId=@Id) and 
(itemfunc.IsDemo=@IsDemo)
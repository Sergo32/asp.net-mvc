﻿CREATE PROCEDURE [Info].[GetHierarchyCategory]
as
WITH
 Rec (Id, ParentId, Title ,Comment, Sort, Level)
 AS (
-- корневая часть
   SELECT Id, ParentId, Title, Comment, Sort, 0 as Level FROM Info.Category
     WHERE ParentId is null
   UNION ALL
-- рекурсивная часть
   SELECT Cat.Id, Cat.ParentId, Cat.Title, Cat.Comment, Cat.Sort, Rec.level+1
    FROM Rec, Info.Category as Cat
    WHERE Rec.id = Cat.ParentId
   )
 SELECT * FROM Rec

 ORDER BY Sort
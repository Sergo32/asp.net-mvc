﻿CREATE PROCEDURE [Info].[GetItems]
	@CategoryId int = null,
	@DatePublication datetime2 = null,
	@UrlToPast varchar(max)= null,
	@UrlToFuture varchar(max) = null,
	@DatePublicationToPastList datetime2= null,
	@DatePublicationToFutureList datetime2 = null,
	@Title nvarchar(max) = null,
	@DemoText nvarchar(max) = null,
	@IsDeleted bit = 0,
	@IsActive bit = null,
	@SortColumnName varchar(max) = 'Id',
	@SortDirection nvarchar(max) = 'desc',
	@Start int = 0,
	@Lenght int = 999999999,
    @Id int=null,
	@ParentId int=null,
	@TypeId int=null,
    @DatePublicationMax datetime2 = null,
	@DatePublicationMin datetime2 = null,
    @DateStartPublicationMax datetime2 = null,
	@DateStartPublicationMin datetime2 = null,
    @DateEndPublicationMax datetime2 = null,
	@DateEndPublicationMin datetime2 = null

as
	declare @TotalRows int =0
	declare @FilteredRows int =0

	DECLARE @idsTable TABLE ( Id int, [no] int, [FilteredRows] int);
	
	
	WITH t1 as (
		SELECT [it].Id, ROW_NUMBER() OVER (order by 
		case when (@SortColumnName=N'Id' and @SortDirection = N'asc') then[it].[Id] end asc,
		case when (@SortColumnName=N'Id' and @SortDirection = N'desc') then [it].[Id] end desc, 
		case when (@SortColumnName=N'Title' and @SortDirection = N'asc') then [it].[Title] end asc,
		case when (@SortColumnName=N'Title' and @SortDirection = N'desc') then [it].[Title] end desc,
		case when (@SortColumnName=N'DatePublication' and @SortDirection = N'asc') then [it].[DatePublication] end asc,
		case when (@SortColumnName=N'DatePublication' and @SortDirection = N'desc') then [it].[DatePublication] end desc,
		case when (@SortColumnName=N'DateStart' and @SortDirection = N'asc') then [it].[DateStart] end asc,
		case when (@SortColumnName=N'DateStart' and @SortDirection = N'desc') then [it].[DateStart] end desc,
		case when (@SortColumnName=N'DateEnd' and @SortDirection = N'asc') then [it].[DateEnd] end asc,
		case when (@SortColumnName=N'DateEnd' and @SortDirection = N'desc') then [it].[DateEnd] end desc
		) as [no]
		From [Info].[Item]  as it inner Join [Info].[ItemCategory] as [ItemCategory] on [ItemCategory].ItemId=it.Id join
			[Info].[Category] as c on  [ItemCategory].CategoryId=c.Id join [Info].[Type] as T on T.Id = it.TypeId

		WHERE
	     (@Id is null or it.Id=@Id)   
			and (@IsDeleted IS NULL OR it.IsDeleted=@IsDeleted) 
			and (@ParentId IS NULL OR c.ParentId=@ParentId) 
			and (@IsActive IS NULL OR it.IsActive=@IsActive) 
			and (@Title IS NULL OR it.Title LIKE ('%' + @Title + '%'))
			and (@DatePublicationMin IS NULL OR it.[DatePublication] >= @DatePublicationMin) 
			and (@DatePublicationMax IS NULL OR it.[DatePublication] <= @DatePublicationMax)  
			and
			(it.[DateStart] IS NULL OR 
					(@DateStartPublicationMin IS NULL OR it.[DateStart] >= @DateStartPublicationMin) 
						and
					(@DateStartPublicationMax IS NULL OR it.[DateStart] <= @DateStartPublicationMax) 
			)
			AND 
			(
				it.[DateEnd] IS NULL
				OR 
					(@DateEndPublicationMin IS NULL OR it.[DateEnd] >= @DateEndPublicationMin) 
						and
					(@DateEndPublicationMax IS NULL OR it.[DateEnd] <= @DateEndPublicationMax) 
			) 
			and (@TypeId IS NULL OR it.TypeId=@TypeId)
			and (@CategoryId is null  or [ItemCategory].CategoryId=@CategoryId) 
			and (@DatePublication is null 
					or (
						((@DatePublication >= it.DateStart) and (@DatePublication <= it.DateEnd)) 
						or ((it.DateStart is null) and (it.DateEnd is null)) 
						or ((it.DateStart is null) and (@DatePublication <= it.DateEnd)) 
						or ((@DatePublication >= it.DateStart) and (it.DateEnd is null))
					)
			) 
			and (@UrlToPast is null or ContentUrl != N'') 
			and (@UrlToFuture is null or ContentUrl = N'') 
			and (@DatePublicationToPastList is null or it.DatePublication <= @DatePublicationToPastList) 
			and (@DatePublicationToFutureList is null or it.DatePublication >= @DatePublicationToFutureList) 
	group by it.Id
		,it.Title
		,it.Text
		,it.Description
		,it.SourceTitle
		,it.SourceUrl
		,it.ContentUrl
		,it.ExtraUrl
		,it.DatePublication
		,it.IsDeleted
		,it.DateDeleted
		, it.DateStart
		, it.DateEnd
		,it.IsActive
		,it.TransliteratedTitle
		,it.TypeId
		,it.DemoText
		,it.PreviewURL

    ),
	t2 AS (SELECT count(*) as FilteredRows FROM t1),
	t3 AS (SELECT [Id], [no] from t1 order by [no] OFFSET @Start ROWS FETCH NEXT @Lenght ROWS ONLY)
	INSERT INTO @idsTable([Id],[no],[FilteredRows])
	SELECT [Id],[no],t2.FilteredRows FROM t3,t2;
	
	SELECT TOP 1 @FilteredRows = [FilteredRows] FROM @idsTable

	SELECT
		it.Id
		,it.Title
		,it.Text
		,it.Description
		,it.SourceTitle
		,it.SourceUrl
		,it.ContentUrl
		,it.ExtraUrl
		,it.DatePublication
		,it.IsDeleted
		,it.DateDeleted
		,it.DateStart
		,it.DateEnd
		,it.IsActive
		,it.TransliteratedTitle
		,it.TypeId
		,it.DemoText
		,it.PreviewURL

	FROM [Info].[Item] AS it inner join @idsTable as ids on ids.Id = [it].Id 
	order by ids.[no]
	
	SELECT @totalRows = COUNT(*) FROM @idsTable
	
	SELECT @TotalRows as TotalRows, @FilteredRows as FilteredRows



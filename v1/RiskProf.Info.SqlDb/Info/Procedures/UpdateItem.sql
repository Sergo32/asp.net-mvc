﻿CREATE PROCEDURE [Info].[UpdateItem]
	@Id int, 
    @Title nvarchar(max) ,
	@Text nvarchar(max) ,
    @Description nvarchar(max) ,
	@SourceTitle nvarchar(max) ,
	@SourceUrl nvarchar(max) ,
	@ContentUrl nvarchar(max) ,
	@ExtraUrl nvarchar(max) ,
	@DatePublication datetime2 ,
	@IsDeleted bit ,
	@DateDeleted datetime2,
	@DateStart datetime2,
	@DateEnd datetime2,
	@TransliteratedTitle nvarchar(max),
	@TypeId int,
	@DemoText nvarchar(max),
	@PreviewURL nvarchar(max)
as

UPDATE [Info].[Item]
SET Title = @Title, Text = @Text ,Description = @Description, SourceTitle = @SourceTitle, SourceUrl = @SourceUrl, ContentUrl = @ContentUrl,ExtraUrl = @ExtraUrl, DatePublication = @DatePublication, IsDeleted = @IsDeleted, DateDeleted = @DateDeleted, DateStart = @DateStart, DateEnd = @DateEnd, TransliteratedTitle=@TransliteratedTitle, TypeId = @TypeId, DemoText=@DemoText ,PreviewURL=@PreviewURL
WHERE Id = @Id
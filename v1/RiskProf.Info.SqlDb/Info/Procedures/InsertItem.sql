﻿CREATE PROCEDURE [Info].[InsertItem]
	@Title nvarchar(max) ,
	@Text nvarchar(max) ,
    @Description nvarchar(max) ,
	@SourceTitle nvarchar(max) ,
	@SourceUrl nvarchar(max) ,
	@ContentUrl nvarchar(max) ,
	@ExtraUrl nvarchar(max) ,
	@DatePublication datetime2 ,
	@IsDeleted bit ,
	@DateDeleted datetime2 ,
	@DateStart datetime2,
    @IsActive bit,
	@DateEnd datetime2,
	@TransliteratedTitle nvarchar(max),
	@TypeId int,
    @DemoText  nvarchar(max) ,
    @PreviewURL nvarchar(max) 
as
begin
INSERT INTO [Info].[Item]
VALUES (@Title,@Text,@Description,@SourceTitle,@SourceUrl,@ContentUrl,@ExtraUrl,@DatePublication,@IsDeleted,@IsActive,@DateDeleted,@DateStart,@DateEnd,@TransliteratedTitle, @TypeId, @DemoText,@PreviewURL)
select cast(scope_identity() as int) AS [scope_identity];
end
﻿
create type TableItemClaim as table (ItemId int,ClaimId int,IsDemo bit);

go
CREATE PROCEDURE [Info].[InsertItemClaim]
    @ids TableItemClaim readonly
as
INSERT INTO [Info].[ItemClaim]
select ItemId,ClaimId,IsDemo from  @ids


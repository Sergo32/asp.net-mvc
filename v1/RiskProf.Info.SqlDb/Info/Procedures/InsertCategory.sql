﻿CREATE PROCEDURE [Info].[InsertCategory]
	@Title nvarchar(max),
    @ParentId int = null,
	@PageId int = null,
	@Sort int = null,
    @Comment nvarchar(max)
	
as
INSERT INTO [Info].[Category]
	([Title],
	[ParentId],
	[PageId],
	[Sort],	
	[Comment]
	) 				
VALUES (@Title,@ParentId,@PageId,@Sort,@Comment)
﻿CREATE PROCEDURE [Info].[GetAllItemCategoryById]
  	@Id int = null
as
select IC.Id,IC.ItemId,IC.CategoryId,C.Title, C.Comment From [Info].[ItemCategory]  as IC join [Info].[Category] as C on IC.CategoryId=C.Id
where IC.ItemId=@Id;
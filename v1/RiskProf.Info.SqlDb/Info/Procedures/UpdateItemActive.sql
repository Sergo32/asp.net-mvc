﻿CREATE PROCEDURE [Info].[UpdateItemActive]
    @Id int,
    @IsActive bit 
as
UPDATE [Info].[Item]  set    IsActive=@IsActive  where [Item].Id=@Id
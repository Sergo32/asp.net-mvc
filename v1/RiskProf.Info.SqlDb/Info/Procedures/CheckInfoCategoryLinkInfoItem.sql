﻿CREATE PROCEDURE [Info].[CheckInfoCategoryLinkInfoItem]
	@CategoryId int	
as
declare @check bit 
if	(EXISTS (select Id from [Info].[ItemCategory] where CategoryId = @CategoryId) ) set @check=1
else  set @check=0
select @check

﻿CREATE TABLE [Info].[Claims]
(
	[Id] int not null identity primary key,	
	[Title] nvarchar(max) not null,
	[AuthKey] nvarchar(128) not null
)
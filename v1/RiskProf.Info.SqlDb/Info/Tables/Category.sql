﻿CREATE TABLE [Info].[Category]
(
    [Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY,	
	[Title] nvarchar(max) not null,	
	[ParentId] int FOREIGN KEY REFERENCES [Info].[Category] (Id),
	[PageId] int FOREIGN KEY REFERENCES [Info].[Page] (Id),	
	[Sort] INT ,
	[Comment] nvarchar(max)
)
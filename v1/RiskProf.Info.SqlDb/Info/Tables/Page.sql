﻿CREATE TABLE [Info].[Page]
(
    [Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	[Guid] uniqueidentifier NOT NULL,
	[Title] nvarchar(max) not null

)
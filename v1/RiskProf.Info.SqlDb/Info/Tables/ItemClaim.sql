﻿CREATE TABLE [Info].[ItemClaim]
(
	[Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	[ItemId] int NOT NULL FOREIGN KEY REFERENCES [Info].[Item] (Id),
    [ClaimId] int NOT NULL, 
	[IsDemo] bit NOT NULL
)
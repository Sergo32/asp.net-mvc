﻿CREATE TABLE [Info].[CategoryPage]
(
	[Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	[CategoryId] int NOT NULL FOREIGN KEY REFERENCES [Info].[Item] (Id),
    [PageId] int NOT NULL FOREIGN KEY REFERENCES [Info].[Category] (Id)

)
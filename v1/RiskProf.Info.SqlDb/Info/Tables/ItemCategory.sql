﻿CREATE TABLE [Info].[ItemCategory]
(
	[Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	[ItemId] int NOT NULL FOREIGN KEY REFERENCES [Info].[Item] (Id),
    [CategoryId] int NOT NULL FOREIGN KEY REFERENCES [Info].[Category] (Id)

)
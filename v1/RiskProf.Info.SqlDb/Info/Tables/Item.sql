﻿CREATE TABLE [Info].[Item]
( 
    [Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY,	
	[Title] nvarchar(max) not null,
	[Text] nvarchar(max) not null,
    [Description] nvarchar(max) not null,
	[SourceTitle] nvarchar(max) not null,
	[SourceUrl] nvarchar(max) not null,
	[ContentUrl] nvarchar(max) not null,
	[ExtraUrl] nvarchar(max) not null,
	[DatePublication] datetime2 not null,
	[IsDeleted] bit not null,
	[IsActive] bit not null,
	[DateDeleted] datetime2,
	[DateStart] datetime2,
	[DateEnd] datetime2, 
    [TransliteratedTitle] nvarchar(200) not null,
	[TypeId] int not null FOREIGN KEY REFERENCES [Info].[Type] (Id),
	[DemoText] nvarchar(max),
	[PreviewURL] nvarchar(max) not null
 

)
﻿CREATE TABLE [Info].[Type]
(
    [Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY,	
	[Title] nvarchar(max) not null,
	[Guid] uniqueidentifier NOT NULL
)
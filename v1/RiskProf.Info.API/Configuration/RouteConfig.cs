﻿namespace RiskProf.Info.Admin.Configuration
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;
    using RiskProf.Auth.Client;
    using RiskProf.Helpers.Routes;
    using RiskProf.Info.Admin.Web.Controllers;
    using RiskProf.Info.Filter;

    public class RouteConfig
    {
        internal static void Configure(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.AddController<InfoItemAPIController>()
                .ActionAction("", x => x.Index)
                 .NeedHttpGet()
                    .JsonAction<int>("ajax/post/infoItemById", x => x.GetInfoItem)
                        .NeedHttpPost()
                    .JsonAction<InfoItemFilter>("ajax/post/infoItemList", x => x.GetInfoItemList)
                        .NeedHttpPost()
                    .JsonAction<Guid>("ajax/post/categories", x => x.GetCategories)
                        .NeedHttpPost();

            


        }
    }
}
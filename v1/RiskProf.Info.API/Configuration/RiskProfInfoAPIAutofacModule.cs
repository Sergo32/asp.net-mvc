﻿namespace RiskProf.Info.Admin.Configuration
{
    using Autofac;
    using RiskProf.Info.Admin.Web.Controllers;
    using RiskProf.Info.Repository;

    public class InfoItemAPIViews
    {
        public string Index { get; set; }
    }

    public class RiskProfInfoAPIAutofacModule: Module
    {
        public string ConnectionString { get; set; }
        public InfoItemAPIViews InfoItemAPIViews { get; set; }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<InfoItemRepository>()
                    .As<IInfoItemRepository>()
                    .WithParameter("connectionString", this.ConnectionString)
                    .SingleInstance();

            builder.RegisterType<InfoItemAPIController>()
                   .WithParameter("indexView", InfoItemAPIViews.Index);
        }
    }
}
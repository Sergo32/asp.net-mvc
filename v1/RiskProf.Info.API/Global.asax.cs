﻿namespace RiskProf.Info.Admin
{
    using System;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Routing;
    using Autofac;
    using Autofac.Integration.Mvc;
    using RiskProf.Helpers;
    using RiskProf.Helpers.Auth;
    using RiskProf.Info.Admin.Configuration;
    using RiskProf.Auth.Client;

    public class Global : HttpApplication
    {
        protected void ApplicationDependencyConfig()
        {

            var infoConfig = new ConfigurationHelper("RiskProf.Info");
            string infoConnectionString = infoConfig.GetConnectionString("Risk");

            // зависимости
            var builder = new ContainerBuilder();
            {
                builder.Register(x => DependencyResolver.Current).As<IDependencyResolver>().SingleInstance();
                builder.Register(x => (IContainer)((AutofacDependencyResolver)DependencyResolver.Current).ApplicationContainer).As<IContainer>().SingleInstance();

               
                builder.RegisterModule(new RiskProfInfoAPIAutofacModule
                {
                    ConnectionString = infoConnectionString,
                    InfoItemAPIViews = new InfoItemAPIViews
                    {
                        Index = "~/Views/Index.cshtml"
                    }
                });
            }
            IContainer container = builder.Build();
            {
                DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            }
        }
        protected void Application_Start(object sender, EventArgs e)
        {
            this.ApplicationDependencyConfig();

            RouteConfig.Configure(RouteTable.Routes);
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}
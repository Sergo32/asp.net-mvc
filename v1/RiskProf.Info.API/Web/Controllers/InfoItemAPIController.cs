﻿namespace RiskProf.Info.Admin.Web.Controllers
{
    using System;
    using System.Linq;
    using System.Web.Mvc;
    using System.Collections.Generic;
    using RiskProf.Helpers;
    using RiskProf.Info.Admin.Web.Models;
    using System.Globalization;
    using RiskProf.Auth.Client;
    using RiskProf.Auth.Client.Dto;
    using RiskProf.Info.Repository;
    using RiskProf.Info.Filter;
    using RiskProf.Info.Dto;
    using RiskProf.Info.Domain;

    public class InfoItemAPIController : Controller
    {
        private readonly IInfoItemRepository _infoItemRepository;

        private readonly string _indexView;

        public InfoItemAPIController(IInfoItemRepository infoItemRepository, string indexView)
        {
            _infoItemRepository = infoItemRepository;
            _indexView = indexView;


        }
        public ActionResult Index()
        {
            return View(_indexView);
        }
        private bool IsInfoItemPureAccessible(int infoItemId)
        {
            var InfoItemFunctionList = _infoItemRepository.GetAllClaimsForItem(new InfoItemFilter { Id = infoItemId });
           return (InfoItemFunctionList.Any(x => x.ClaimId==5));
        }


        private bool IsInfoItemDemoAccessible(int infoItemId)
        {
            var InfoItemFunctionList = _infoItemRepository.GetAllClaimsForItem(new InfoItemFilter { Id = infoItemId, IsDemo = true });
            return (InfoItemFunctionList.Any(x => x.ClaimId==5));
        }

        private List<InfoItemJSON> IsInfoItemPureAccessible(List<InfoItemJSON> infoItems)
        {
            List<InfoItemJSON> res = new List<InfoItemJSON>();
            foreach (var infoItem in infoItems)
            {
                if ((infoItem.IsOpened = IsInfoItemPureAccessible((int)infoItem.Id)) || (infoItem.IsDemo = IsInfoItemDemoAccessible((int)infoItem.Id)))
                    res.Add(infoItem);
            }
            return res;
        }

        public JsonResult GetInfoItem(int id)
        {
            var infoItem = _infoItemRepository.GetItems(new InfoItemFilter { Id=id}).Select(item => new InfoItemJSON(item)).FirstOrDefault();

            // TODO: сделать проверку анонима
            if (infoItem != null && (infoItem.IsOpened = IsInfoItemPureAccessible((int)infoItem.Id) || infoItem != null && (infoItem.IsDemo=IsInfoItemDemoAccessible((int)infoItem.Id))))
                return Json(infoItem, JsonRequestBehavior.AllowGet);

            Response.WriteError("InfoItem not found");
            return null;
        }

        public JsonResult GetInfoItemList(InfoItemFilter filter = null)
        {

            List<InfoItemJSON> infoItems = new List<InfoItemJSON>();
            

            //если фильтр или страницп  не заданы, то возращаем пустой список.
            if (filter == null || filter.Guid == null) return Json(infoItems, JsonRequestBehavior.AllowGet);

            List<InfoItemJSON> infoItemsTemp = new List<InfoItemJSON>();
            List<InfoItemCategory> infoItemCategories = new List<InfoItemCategory>();

            infoItemCategories = _infoItemRepository.GetAllCategory(new InfoItemFilter { Guid = filter.Guid }).ToList();
            filter.Guid = null;

            //если присланая категория не соответствует присланой странице, то возращаем пустой список.
            if (filter.CategoryId != null && !infoItemCategories.Any(x => x.CategoryId == filter.CategoryId))
                return Json(infoItems, JsonRequestBehavior.AllowGet);

            if (filter.CategoryId != null)
            {
                infoItemsTemp = _infoItemRepository.GetItems(filter).Select(item => new InfoItemJSON(item)).ToList();
            }
            else
            {
                foreach (var category in infoItemCategories)
                {
                    filter.CategoryId = category.CategoryId;
                    foreach (var itemTemp in _infoItemRepository.GetItems(filter).Select(item => new InfoItemJSON(item)).ToList())
                    {
                        infoItemsTemp.Add(itemTemp);
                    }
                }
            }

            //Избавляемся от дублирования инфоблоков, попутно преобразуем даты.
            foreach (var itemTemp in infoItemsTemp)
            {
                if (infoItems.Any(x => x.Id == itemTemp.Id) == false)
                {
                    if (DateTime.TryParseExact(itemTemp.DatePublication, "dd.MM.yyyy HH:mm", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime date))
                        itemTemp.SortDatePublication = date;

                    infoItems.Add(itemTemp);
                }
            }

            //Добавляем сведения о категориях и типе инфоблоков.
            for (int i = 0; i < infoItems.Count; i++)
            {
                infoItems[i].ItemCategoriesList = _infoItemRepository.GetAllItemCategoryById(new InfoItemFilter { Id = infoItems[i].Id });
                infoItems[i].Type = _infoItemRepository.GetAllType(new InfoItemFilter { TypeId = infoItems[i].TypeId }).FirstOrDefault();
            }

            //оставляем в списке только доступные инфоблоки.
            infoItems = IsInfoItemPureAccessible(infoItems);
            infoItems = infoItems.OrderByDescending(x => x.SortDatePublication).ToList();

            if (filter.CountLengths != null)
                infoItems = infoItems.Take((int)filter.CountLengths).ToList();//отдаем 3 новости если есть фильтр

            return Json(infoItems, JsonRequestBehavior.AllowGet);

        }

        public JsonResult GetCategories(Guid pageGuid)
        {
            if (pageGuid == null)
                return Json(null, JsonRequestBehavior.AllowGet);

            var CategoryModel = new CategoryInfoItemModel()
            {
                InfoItemCategoryList = _infoItemRepository.GetAllCategory(new InfoItemFilter { Guid = pageGuid }).ToList()
            };

            return Json(CategoryModel, JsonRequestBehavior.AllowGet);
        }

    }
}
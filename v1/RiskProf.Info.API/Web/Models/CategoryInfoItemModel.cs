﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RiskProf.Info.Admin.Web.Models
{
    //public class ViewModelBase : ICabinetSiteModel
    //{
    //    public ITeuAuthorizationService AuthorizationService { get; set; }
    //    public IAuthenticationService AuthenticationService { get; set; }
    //    public UrlHelper UrlHelper { get; set; }
    //    public HtmlHelper HtmlHelper { get; set; }
    //    public string ModuleName { get; set; }
    //    public string Layout { get; set; }
    //    public int? Id { get; set; }
    //    public bool WithoutCabinet { get; set; }
    //    public bool IsCabinet { get; set; }
    //    public bool IsWorkspace { get; set; }
    //    public int? CabinetId { get; set; }
    //    public int? SiteId { get; set; }
    //    public int? WorkSpaceId { get; set; }
    //    public string ReturnUrl { get; set; }
    //    public string Title { get; set; }
    //    public string ActiveTab { get; set; }
    //    public bool ShowTabs { get; set; }
    //    public int? ObjectTypeId { get; set; }
    //    public ControllerContext ControllerContext { get; set; }

    //    public Dictionary<string, string> BrendParamenters { get; set; }

    //    public string RouteNameFull(string routeName)
    //    {
    //        return RouteNameFull(this.ModuleName, routeName);
    //    }

    //    public string RouteNameFull(string moduleName, string routeName)
    //    {
    //        return "{0}_{1}".ApplyFormat(moduleName, routeName);
    //    }

    //    public string GetUrlFromRout(UrlHelper urlHelper, string name, object routeValues)
    //    {
    //        RouteValueDictionary p = new RouteValueDictionary(routeValues);

    //        if (!this.WithoutCabinet && !p.Keys.Contains("cabinetId"))
    //            p.Add("cabinetId", this.CabinetId);

    //        var fullName = RouteNameFull(name);

    //        VirtualPathData vpd = RouteTable.Routes.GetVirtualPathForArea(urlHelper.RequestContext, fullName, p);
    //        var vpna = RouteTable.Routes.GetVirtualPath(urlHelper.RequestContext, fullName, p);

    //        return urlHelper.RouteUrl(fullName, p);
    //    }

    //    public string GetUrlFromRout(UrlHelper urlHelper, string moduleName, string name, object routeValues)
    //    {
    //        RouteValueDictionary p = new RouteValueDictionary(routeValues);

    //        if (!this.WithoutCabinet && this.IsCabinet)
    //            p.Add("cabinetId", this.CabinetId);

    //        var fullName = RouteNameFull(moduleName, name);
    //        var result = urlHelper.RouteUrl(fullName, p);
    //        return result;
    //    }


    //    public MvcForm BeginRouteForm(HtmlHelper helper, string moduleName, string name, FormMethod method, object routeValues, object httpAttributes)
    //    {
    //        return helper.BeginRouteForm(RouteNameFull(moduleName, name), routeValues, method, httpAttributes);
    //    }
    //    public MvcForm BeginRouteForm(HtmlHelper helper, string moduleName, string name, FormMethod method, object routeValues)
    //    {
    //        RouteValueDictionary p = new RouteValueDictionary(routeValues);
    //        if (!this.WithoutCabinet && this.IsCabinet)
    //            p.Add("cabinetId", this.CabinetId);

    //        return helper.BeginRouteForm(RouteNameFull(moduleName, name), p, method);
    //    }
    //    public MvcForm BeginRouteForm(HtmlHelper helper, string moduleName, string name, FormMethod method)
    //    {
    //        return helper.BeginRouteForm(RouteNameFull(moduleName, name), method);
    //    }

    //    public MvcForm BeginRouteForm(HtmlHelper helper, string name, FormMethod method, object routeValues, object httpAttributes)
    //    {
    //        return helper.BeginRouteForm(RouteNameFull(name), routeValues, method, httpAttributes);
    //    }
    //    public MvcForm BeginRouteForm(HtmlHelper helper, string name, FormMethod method, object routeValues)
    //    {
    //        RouteValueDictionary p = new RouteValueDictionary(routeValues);
    //        if (!this.WithoutCabinet && this.IsCabinet)
    //            p.Add("cabinetId", this.CabinetId);

    //        return helper.BeginRouteForm(RouteNameFull(name), p, method);
    //    }
    //    public MvcForm BeginRouteForm(HtmlHelper helper, string name, FormMethod method)
    //    {
    //        return helper.BeginRouteForm(RouteNameFull(name), method);
    //    }

    //    public MvcHtmlString RouteAction(HtmlHelper htmlHelper, string name, object routeValues)
    //    {
    //        RouteValueDictionary p = new RouteValueDictionary(routeValues);

    //        if (!this.WithoutCabinet && this.IsCabinet)
    //            p.Add("cabinetId", this.CabinetId);

    //        string routeName = RouteNameFull(name);
    //        RouteBase route = htmlHelper.RouteCollection[routeName];
    //        var action = (route as Route).Defaults["action"] as string;
    //        var controller = (route as Route).Defaults["controller"] as string;
    //        return htmlHelper.Action(action, controller, p);
    //    }

    //    public MvcHtmlString RouteAction(HtmlHelper htmlHelper, string moduleName, string name, object routeValues)
    //    {
    //        RouteValueDictionary p = new RouteValueDictionary(routeValues);

    //        if (!this.WithoutCabinet && this.IsCabinet)
    //            p.Add("cabinetId", this.CabinetId);

    //        string routeName = RouteNameFull(moduleName, name);
    //        RouteBase route = htmlHelper.RouteCollection[routeName];
    //        var action = (route as Route).Defaults["action"] as string;
    //        var controller = (route as Route).Defaults["controller"] as string;
    //        return htmlHelper.Action(action, controller, p);
    //    }

    //    public bool VerifyAccess(string domain, string routeName)
    //    {
    //        return AuthorizationService.HasPermission(domain, this.ModuleName, routeName, this.ControllerContext);
    //    }

    //    public bool VerifyAccess(string domain, string moduleName, string routeName)
    //    {
    //        return AuthorizationService.HasPermission(domain, moduleName, routeName, this.ControllerContext);
    //    }
    //}
    public class CategoryInfoItemModel //: ViewModelBase
    {
        public IEnumerable<Domain.InfoItemCategory> InfoItemCategoryList { get; set; }
        //public InfoItemCategory Parent { get; set; }
    }
}
﻿using RiskProf.Auth.Client;
using RiskProf.Auth.Client.Dto;
using RiskProf.Helpers;
using RiskProf.Info.Domain;
using RiskProf.Info.Dto;
using RiskProf.Info.Filter;
using RiskProf.Info.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RiskProf.Info.Admin.Web.Controllers
{
    //временно для вывода категорий static class MyToSelectList
    #region MyToSelectList  
    /// <summary>
    /// Преобразует перечень объектов в перечень элементов html-списка
    /// </summary>
    /// <typeparam name="T">Тип преобразуемого объекта</typeparam>
    /// <param name="items">Перечень преобразуемых объектов</param>
    /// <param name="valueSelector">Функция, возвращающая значение, используемое в качестве Value для элемента списка.
    /// Для преобразования возвращаемого значения в строку вызывается стантартная реализация ToString()</param>
    /// <param name="textSelector">Функция, возвращающая значение, используемое в качестве Text для элемента списка.
    /// Для преобразования возвращаемого значения в строку вызывается стантартная реализация ToString()</param>
    /// 

    public static class MyToSelectList
    {
        public static IEnumerable<SelectListItem> ToSelectList<T>(this IEnumerable<T> items, Func<T, object> valueSelector, Func<T, object> textSelector)

        {
            if (valueSelector == null) throw new ArgumentNullException("valueSelector");
            if (textSelector == null) throw new ArgumentNullException("textSelector");

            return items.Select(i => new SelectListItem
            {
                Value = valueSelector(i).ToString(),
                Text = textSelector(i).ToString()
            });
        }
    }
    #endregion//временно для вывода категорий //временно для вывода категорий

    public class InfoAdminController : Controller
    {
        private InfoItemModel model;
        private readonly string _noAccessView;
        private readonly IInfoItemRepository _infoItemRepository;
        private readonly string _adminPanelView;
        private readonly string _editView;
        private readonly string _headerView;
        private readonly string _navMenuView;
        public InfoAdminController(IInfoItemRepository infoItemRepository,            
            string adminPanelView, 
            string editView, 
            string headerView, 
            string navMenuView, 
            string noAccessView)
        {
            _infoItemRepository = infoItemRepository;
            _adminPanelView = adminPanelView;
            _editView = editView;
            _headerView = headerView;
            _navMenuView = navMenuView;
            _noAccessView = noAccessView;
        }

        public PartialViewResult Header()
        {
            //HeaderModel headerModel = new HeaderModel();
            //headerModel.AuthorizationService = _authorizationService;
            return this.PartialView(_headerView);
            //return this.PartialView(_headerMobileView, headerModel);
        }

        public ActionResult NavMenu()
        {
            //var model = new HomeHeaderModel { KbCabinetId = _kbCabinetId };
            //FillModel(model);
            //NavMenuModel navMenuModel = new NavMenuModel();
            //navMenuModel.AuthorizationService = _authorizationService;
            //return View(_navMenuMobileView, navMenuModel);
            return View(_navMenuView);
        }

        [HttpPost]
        [ValidateInput(false)]
        public JsonResult SaveInfoItem(InfoItemJSON item)//  HttpPostedFileBase d
        {
            try
            {
                if (DateTime.TryParseExact(item.DatePublication, "dd.MM.yyyy HH:mm", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime date) == false)
                {
                    return Json(new { State = 1, Message = "Отображаемая дата некорректная" });
                }

                InfoItem infoItem = new InfoItem();
                bool check = false;
                item.TransliteratedTitle = item.Title.TranslitToEnglish();
                if (item.TransliteratedTitle.Length > 200)
                    item.TransliteratedTitle = item.TransliteratedTitle.Substring(0, 200);
                if (item.Id != null && item.CategoryIds[0] != null)
                {
                    infoItem = _infoItemRepository.GetItems(new InfoItemFilter { Id = item.Id }).FirstOrDefault();
                    //Kiout.Core.Debugger.Check.IsNotNull(infoItem, "Инфоблок не найден");
                    infoItem = new InfoItem(item);//Присвоили изменения этому элементу
                    // Вычищаем и заполняем функции у инфоблока
                    _infoItemRepository.DeleteItemClaimById(item.Id);
                    _infoItemRepository.DeleteItemCategoryById(item.Id);

                }
                else if (item.CategoryIds[0] != null)
                {
                    infoItem = new InfoItem(item);
                    item.Id = _infoItemRepository.InsertItem(infoItem);//получаем id  элемента
                    check = true;
                }

                _infoItemRepository.UpdateItemActive(item.Id, (item.AvailableOn == "on") ? true : false);

                DataTable dataItemClaim = new DataTable();
                dataItemClaim.Columns.Add("ItemId", typeof(int));
                dataItemClaim.Columns.Add("ClaimId", typeof(int));
                dataItemClaim.Columns.Add("IsDemo", typeof(bool));

                if (!item.ClaimIds.IsNullOrEmpty())
                {
                    for (int i = 0; i < item.ClaimIds.Length; i++)
                    {
                        dataItemClaim.Rows.Add(item.Id, item.ClaimIds[i], false);
                    }
                }
                if (!item.ClaimBool.IsNullOrEmpty())
                {
                    for (int i = 0; i < item.ClaimBool.Length; i++)
                    {
                        dataItemClaim.Rows.Add(item.Id, item.ClaimBool[i], true);
                    }
                }

                if (dataItemClaim.Rows.Count > 0)
                    _infoItemRepository.InsertItemClaim(dataItemClaim);

                if (!infoItem.Text.IsNullOrEmpty())
                    infoItem.Text = Uri.UnescapeDataString(infoItem.Text);

                if (!infoItem.DemoText.IsNullOrEmpty())
                    infoItem.DemoText = Uri.UnescapeDataString(infoItem.DemoText);

                if (item.Id != null && item.CategoryIds[0] != null && check == false)
                    _infoItemRepository.UpdateItem(infoItem);

                DataTable dataItemCategory = new DataTable();
                dataItemCategory.Columns.Add("ItemId", typeof(int));
                dataItemCategory.Columns.Add("CategoryId", typeof(int));

                for (int i = 0; i < item.CategoryIds.Length; i++)
                {
                    dataItemCategory.Rows.Add(item.Id, item.CategoryIds[i]);
                }
                _infoItemRepository.InsertItemCategory(dataItemCategory);
            }
            catch (InvalidOperationException e)
            {
                return Json(new { State = 1, Message = "Элемент не добавлен." + e.Message });
            }
            catch (Exception e)
            {
                return Json(new { State = 500, Message = "Элемент не добавлен. На сервере произошла неизвестная ошибка" + e.Message });
            }
            return Json(new { State = 0 });
        }

        [HttpGet]
        public ActionResult EditInfoItem(int? itemInfoId)
        {         
            model = new InfoItemModel();
            model.Claims = _infoItemRepository.GetAllClaims(new InfoItemFilter { });
            model.Types = _infoItemRepository.GetAllType(new InfoItemFilter { });

            InfoItemFilter filter = new InfoItemFilter();
            model.UserAvaliable = true;
            //model.UserAvaliable = _currentUser.IsMainAdmin; // TODO: ПОЧИНИТЬ ПОЛЬЗОВАТЕЛЯ
            List<InfoItemClaim> Claims = new List<InfoItemClaim> { };
            if (itemInfoId != null)
            {
                model.InfoItemToEdit = _infoItemRepository.GetItems(new InfoItemFilter { Id = itemInfoId }).Select(item => new InfoItemJSON(item)).FirstOrDefault();
                model.InfoItemCategoryList = _infoItemRepository.GetAllCategory(filter);
                model.InfoItemToEdit.ItemCategoriesList = _infoItemRepository.GetAllItemCategoryById(new InfoItemFilter { Id = itemInfoId });
                model.InfoItemToEdit.ClaimsList = _infoItemRepository.GetAllClaimsForItem(new InfoItemFilter { Id = itemInfoId });
                model.InfoItemToEdit.ClaimsListDemo = _infoItemRepository.GetAllClaimsForItem(new InfoItemFilter { Id = itemInfoId, IsDemo = true });

                //model.Files = _entityFileService.GetFiles((int)EntityTypes.InfoItem, itemInfoId.Value);
            }
            else
            {
                model.InfoItemToEdit = new InfoItemJSON { };
                model.InfoItemToEdit.ItemCategoriesList = _infoItemRepository.GetAllItemCategoryById(new InfoItemFilter { Id = itemInfoId });
                model.InfoItemCategoryList = _infoItemRepository.GetAllCategory(filter);
                model.InfoItemToEdit.ClaimsList = Claims;
                model.InfoItemToEdit.ClaimsListDemo = Claims;
            }

            return View(_editView, model);
        }

        public ActionResult InfoItemAdminPanel()
        {          
            model = new InfoItemModel();
            InfoItemFilter filter = new InfoItemFilter();

            List<InfoItemJSON> infoItems = new List<InfoItemJSON>();

            model.InfoItemList = _infoItemRepository.GetItems(filter).Select(item => new InfoItemJSON(item));
            infoItems = model.InfoItemList.ToList();
            for (int i = 0; i < infoItems.Count; i++)
            {
                infoItems[i].ItemCategoriesList = _infoItemRepository.GetAllItemCategoryById(new InfoItemFilter { Id = infoItems[i].Id });
            }

            model.InfoItemList = infoItems;
            //model.UserAvaliable = _currentUser.IsMainAdmin;
            model.UserAvaliable = true;

            return View(_adminPanelView, model);
        }

        [HttpPost]
        public ActionResult GetCategoryList()
        {
            var list = _infoItemRepository.GetHierarchyCategory(new InfoItemFilter { });
            string spaceHtml = "&nbsp;&nbsp;";

            foreach (var item in list)
            {
                item.TitleCategory = item.InnerName;
            }

            foreach (var item in list)
            {
                if (item.Level != 0)
                {
                    for (int i = 0; i < item.Level; i++)
                    {
                        item.TitleCategory = spaceHtml + item.TitleCategory;
                    }
                }
            }          
            // return null;
            return Json(MyToSelectList.ToSelectList(list, x => x.CategoryId, x => x.TitleCategory).InsertFirstToNewList(new SelectListItem { Value = "", Text = "Не выбрано" }));
        }
 

        public JsonResult GetInfoItemAdminPanel(InfoItemFilter infoItemFilter)
        {
            model = new InfoItemModel();
            InfoItemFilter filter = new InfoItemFilter
            {
                Id = infoItemFilter.Id,
                Title = infoItemFilter.Title,
                CategoryId = infoItemFilter.CategoryId,
                SortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"],
                SortDirection = Request["order[0][dir]"],
                Start = Request["start"].TryParseToInt32(0),
                Lenght = Request["length"].TryParseToInt32(10),
                DatePublicationMax = infoItemFilter.DatePublicationMax,
                DatePublicationMin = infoItemFilter.DatePublicationMin,
                DateStartPublicationMax = infoItemFilter.DateStartPublicationMax,
                DateStartPublicationMin = infoItemFilter.DateStartPublicationMin,
                DateEndPublicationMax = infoItemFilter.DateEndPublicationMax,
                DateEndPublicationMin = infoItemFilter.DateEndPublicationMin,
                IsActive = null
            };

            int TotalRow = 0;
            int FilterRow = 0;

            List<InfoItemJSON> infoItems = new List<InfoItemJSON>();

            model.InfoItemList = _infoItemRepository.GetItems(filter).Select(item => new InfoItemJSON(item));
            infoItems = model.InfoItemList.ToList();
            for (int i = 0; i < infoItems.Count; i++)
            {
                infoItems[i].ItemCategoriesList = _infoItemRepository.GetAllItemCategoryById(new InfoItemFilter { Id = infoItems[i].Id });
            }

            if (infoItems.Count() > 0)
            {
                TotalRow = infoItems.First().TotalRow;
                FilterRow = infoItems.First().FilterRow;
            }

            return Json(new
            {
                data = infoItems,
                draw = Request["draw"],
                recordsTotal = TotalRow,
                recordsFiltered = FilterRow,
                riskObjectTypeTitle = "",
                isWorkplace = true
            }, JsonRequestBehavior.AllowGet);
        }

        #region Delete/Active
        [HttpGet]
        public ActionResult DeleteInfoItem(int? id)
        {

            _infoItemRepository.UpdateItemIsDeleted(id, true, DateTime.Now);
            return RedirectToAction("InfoItemAdminPanel", "InfoItem");
        }

        [HttpGet]
        public ActionResult InfoItemActive(int? id, int isActive)
        {
            _infoItemRepository.UpdateItemActive(id, (isActive == 1) ? false : true);
            return RedirectToAction("InfoItemAdminPanel", "InfoItem");
        }
        #endregion
    }
}
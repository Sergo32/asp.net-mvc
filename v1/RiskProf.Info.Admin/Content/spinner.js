﻿function SpinnerFadeIn(text, time) {
	if (text === undefined || text === "") {
		$('.spinner_text').html("");
	}
	else $('.spinner_text').html(text);
	if (time === undefined || time === null) {
		$('.spinner').fadeIn();
	}
	else $('.spinner').fadeIn(time);
}

function SpinnerFadeOut(time) {
	if (time === undefined || time === null) {
		$('.spinner').fadeOut('fast');
	}
	else $('.spinner').fadeOut(time);
}
﻿
TaskResult = {};

TaskResult.Result = function (url, cabinetId, versionId, workplaceGuid, reportId, unitGuid, reportFormat, isDownload) {
    var data = {
        cabinetId: cabinetId,
        versionId: versionId,
        workplaceGuid: workplaceGuid,
        reportId: reportId,
        unitGuid: unitGuid,
        reportFormat: reportFormat
    };

    var taskResultObj = this;
    taskResultObj.taskId = 0;
    taskResultObj.taskStatusId = 0;
    taskResultObj.wait = false;
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        async: false,
        success: function (result) {
            var taskId = result['TaskId'];
            var taskStatusId = result['Status'];
            var wait = result['Wait'];

            taskResultObj.taskId = taskId;
            taskResultObj.taskStatusId = taskStatusId;
            taskResultObj.isDownload = isDownload;
            taskResultObj.wait = wait;

            $.fancybox.open({
                content: '<div style="width: 100%; heignt: 100%;">' + result['Content'] + '</div>',
                'autoSize': false,
                'width': '1300px',
                'height': '100%',
                'beforeClose': function () {
                    taskResultObj.taskId = 0;
                    taskResultObj.taskStatusId = 0;
                    taskResultObj.isDownload = false;
                    taskResultObj.wait = false;
                },
                helpers: {
                    overlay: { locked: false }
                },
                centerOnScroll: true,
                autoCenter: true
            });
        },
        error: function (xhr, status, p3) {
            alert(xhr.responseText);
        },
        complete: function (xhr, status, p3) {
        }
    });
    if (reportFormat === "show" && workplaceGuid !== null && (taskResultObj.taskStatusId !== 3 || taskResultObj.taskStatusId !== 4)) {
        console.log("waiting");
        taskResultObj.TimerId = setInterval(taskResultObj.GetReport, 5000, taskResultObj);
    }
    else if (reportFormat === "show" && (taskResultObj.taskStatusId === 1 || taskResultObj.taskStatusId === 2 || taskResultObj.taskStatusId === null || taskResultObj.taskStatusId === 0)) {
        taskResultObj.TimerId = setTimeout(taskResultObj.GetReport, 20000, taskResultObj);
    }
    else if (taskResultObj.wait !== false && (taskResultObj.taskStatusId !== 3 || taskResultObj.taskStatusId !== 4)) {
        taskResultObj.TimerId = setTimeout(taskResultObj.GetReport, 5000, taskResultObj);
    }
};

TaskResult.Result.prototype.GetReport = function (object) {
    var data = {
        taskId: object.taskId,
        isDownload: object.isDownload
    };
    this.taskId = object.taskId;
    var url = "task/GetTaskResult";
    if (this.taskId !== null && this.taskId !== 0) {
        $.post(url, data, function (data, textStatus, jqXHR) {
            this.taskStatusId = data['Status'];
            var wait = data['Wait'];
            var fancyContent = $(".fancybox-inner");
            if ((this.taskStatusId === 3 || this.taskStatusId === 4) && wait !== false) {
                this.content = '<div style="all: initial; width: 100%; heignt: 100%;">' + data['Content'] + '</div>';
            }
            else {
                this.content = '<div style="vertical-align: middle;">' + data['Content'] + '</div>';
            }
            if ((this.taskStatusId === 1 || this.taskStatusId === 2) && wait === true) {
                console.log("waiting");
            }
            else {
                fancyContent.html(this.content);
                clearInterval(object.TimerId);
            }
        }, 'json').fail(function (xhr, status, error) {

        });
    }
};

TaskResult.Document = function (url, cabinetId, versionId, workplaceGuid, reportId, unitGuid, reportFormat) {
    var data = {
        cabinetId: cabinetId,
        versionId: versionId,
        workplaceGuid: workplaceGuid,
        reportId: reportId,
        unitGuid: unitGuid,
        reportFormat: reportFormat
    };

    var taskId = 0;
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        async: false,
        success: function (result) {
            $.fancybox.open({
                content: result['Content'],
                'autoSize': false,
                'width': '1300px',
                'height': '100%',
                helpers: {
                    overlay: { locked: false }
                },
                centerOnScroll: true,
                autoCenter: true
            });
        },
        error: function (xhr, status, p3) {
            alert(xhr.responseText);
        },
        complete: function (xhr, status, p3) {
        }
    });
    this.taskId = taskId;
    this.taskStatusId = null;
};

TaskResult.CardBase = function (url, data, urlGetResult) {
    console.log("TaskResult.CardBase");
    var taskCardBaseObj = this;
    taskCardBaseObj.taskId = 0;
    taskCardBaseObj.taskStatusId = 0;
    taskCardBaseObj.wait = true;
    taskCardBaseObj.isDownload = false;
    taskCardBaseObj.urlGetResult = urlGetResult;
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        async: false,
        success: function (result) {
            var taskId = result['TaskId'];
            var taskStatusId = result['Status'];

            taskCardBaseObj.taskId = taskId;
            taskCardBaseObj.taskStatusId = taskStatusId;
            taskCardBaseObj.wait = true;

            $.fancybox.open({
                content: '<div style="width: 100%; heignt: 100%;">' + result['Content'] + '</div>',
                'autoSize': false,
                'width': '1300px',
                'height': '100%',
                'beforeClose': function () {
                    taskCardBaseObj.taskId = 0;
                    taskCardBaseObj.taskStatusId = 0;
                    taskCardBaseObj.isDownload = false;
                    taskCardBaseObj.wait = false;
                },
                helpers: {
                    overlay: { locked: false }
                },
                centerOnScroll: true,
                autoCenter: true
            });
        },
        error: function (xhr, status, p3) {
            alert(xhr.responseText);
        },
        complete: function (xhr, status, p3) {
        }
    });
    if (taskCardBaseObj.taskStatusId !== 3 || taskCardBaseObj.taskStatusId !== 4) {
        console.log("waiting card from base");
        taskCardBaseObj.TimerId = setInterval(taskCardBaseObj.GetReport, 5000, taskCardBaseObj);
    }
};

TaskResult.CardBase.prototype.GetReport = function (object) {
    var data = {
        taskId: object.taskId,
        isDownload: object.isDownload
    };
    this.taskId = object.taskId;
    var url = object.urlGetResult;
    console.log(data);
    if (this.taskId !== null && this.taskId !== 0) {
        $.post(url, data, function (data, textStatus, jqXHR) {
            this.taskStatusId = data['Status'];
            var wait = data['Wait'];
            var fancyContent = $(".fancybox-inner");
            if ((this.taskStatusId === 3 || this.taskStatusId === 4) && wait !== false) {
                this.content = '<div style="all: initial; width: 100%; heignt: 100%;">' + data['Content'] + '</div>';
            }
            else {
                this.content = '<div style="vertical-align: middle;">' + data['Content'] + '</div>';
            }
            if ((this.taskStatusId === 1 || this.taskStatusId === 2) && wait === true) {
                console.log("waiting");
            }
            else {
                fancyContent.html(this.content);
                clearInterval(object.TimerId);
            }
        }, 'json').fail(function (xhr, status, error) {

        });
    }
    else {
        clearInterval(object.TimerId);
    }
};
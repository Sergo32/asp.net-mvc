'use strict';
 
var startDelay = 0.3;
function animate(container, animation, delay) {
  if (typeof(delay) === 'undefined') delay = 0;
  var el;
  if (typeof(container) === 'object') {
    el = container;
  } else {
    el = document.querySelectorAll(container);
  }
  if (!el) return undefined;
  for (var i = 0; i < el.length; i++) {
    new Waypoint({
      element: el[i],
      handler: function(direction) {
        delay = delay + startDelay;
        this.element.style.animationDelay = delay + 's';
        this.element.classList.add('animated', animation);
      },
      offset: '100%'
    })
  }
}

$(document).on('ready', function(){

  var animationEnabled = $('body').hasClass('device-desktop');

  if (animationEnabled) {

    //block intro
    if ($('.b-intro__content.js-animate').length) {
      new Waypoint({
        element: $('.b-intro__content.js-animate')[0],
        handler: function() {
          var $this = $(this.element);
          $this.removeClass('js-animate');
          $this.find('.b-intro__title').addClass('animated fadeInDown').css('animation-delay', startDelay + 's');
          $this.find('> p').addClass('animated fadeInUp').css('animation-delay', startDelay + 0.5 + 's');
          $this.find('.b-searchBar').addClass('animated fadeInLeft').css('animation-delay', startDelay + 0.8 + 's');
          $this.find('.b-intro__more').addClass('animated fadeInRight').css('animation-delay', startDelay + 0.8 + 's');
        },
        offset: '100%'
      });
    }

    if ($('.b-access.js-animate').length) {
      //block access
      new Waypoint({
        element: $('.b-access.js-animate')[0],
        handler: function() {
          var $this = $(this.element);
          $this.removeClass('js-animate');
          $this.find('.b-title').addClass('animated fadeInLeft').css('animation-delay', startDelay + 's');
          $this.find('.b-access__content > p').addClass('animated fadeInLeft').css('animation-delay', startDelay + 0.2 + 's');
          $this.find('.b-access__content > ul').addClass('animated fadeInLeft').css('animation-delay', startDelay + 0.4 + 's');
          $this.find('.b-digitalShop').addClass('animated fadeInUp').css('animation-delay', startDelay + 0.6 + 's');

          $this.find('.item-pc').addClass('animated fadeInDown').css('animation-delay', startDelay + 0.4 + 's');
          $this.find('.item-laptop').addClass('animated fadeInUp').css('animation-delay', startDelay + 0.6 + 's');
          $this.find('.item-tablet').addClass('animated fadeInRight').css('animation-delay', startDelay + 0.8 + 's');
        },
        offset: '100%'
      });
    }

    if ($('.b-partners.js-animate').length) {
    //partners access
      new Waypoint({
        element: $('.b-partners.js-animate')[0],
        handler: function() {
          var $this = $(this.element);
          $this.removeClass('js-animate');
          $this.find('.b-title').addClass('animated fadeInLeft').css('animation-delay', startDelay + 's');
          $this.find('.b-partners__content > p').addClass('animated fadeInLeft').css('animation-delay', startDelay + 0.2 + 's');
          $this.find('.b-partners__buttons > .b-btn--white').addClass('animated fadeInLeft').css('animation-delay', startDelay + 0.4 + 's');
          $this.find('.b-partners__link').addClass('animated zoomIn').css('animation-delay', startDelay + 0.4 + 's');
          $this.find('.b-partners__bg').addClass('animated zoomIn').css('animation-delay', startDelay + 0.4 + 's');
        },
        offset: '100%'
      });
    }

    $('.b-title.js-animate').each(function(){
      animate($(this), 'fadeInLeft');
    });

    var effects = [
      'fadeInLeft',
      'zoomIn',
      'fadeInRight'
    ];

    $('.b-adv-item.js-animate').each(function(i){
      animate($(this), effects[i % 3]);
    });

    $('.b-popular .l-grid').each(function(){
      $(this).find('.l-col-33.js-animate').each(function(i){
        animate($(this), effects[i % 3]);
      });
      $(this).find('.l-col-50.js-animate').each(function(i){
        var effect = i % 2 ? effects[2] : effects[0];
        animate($(this), effect);
      });
      $(this).find('.l-col-100.js-animate').each(function(i){
        animate($(this), 'fadeInUp');
      });
    });

    $('.b-news-item.js-animate').each(function(i){
      var effect = i % 2 ? effects[2] : effects[0];
      animate($(this), effect);
    });

    if ($('.b-course-content__list.js-animate').length) {
      new Waypoint({
        element: $('.b-course-content__list.js-animate')[0],
        handler: function() {
          $(this.element).removeClass('js-animate');
        },
        offset: '100%'
      });
    }

    $('.b-content.js-animate').each(function(i){
      animate($(this), 'fadeInRight');
    });

    if ($('.b-sCourse.js-animate').length) {
      new Waypoint({
        element: $('.b-sCourse.js-animate')[0],
        handler: function() {
          $(this.element).removeClass('js-animate');
        },
        offset: '100%'
      });
    }

    $('.b-sCourse__option').each(function(i){
      var delay = i * 0.35;
      animate($(this), 'fadeInDown', delay);
    })
    animate($('.b-sCourse__title'), 'fadeInLeft');
    animate($('.b-sCourse__descr'), 'fadeInLeft', 0.2);

    animate($('.b-buy.js-animate'), 'fadeInRight');
    animate($('.b-video.js-animate'), 'fadeInLeft');

    if ($('.b-sCourse-details__text.js-animate').length) {
    //partners access
      new Waypoint({
        element: $('.b-sCourse-details__text.js-animate')[0],
        handler: function() {
          $(this.element).addClass('animated fadeInLeft').css('animation-delay', startDelay + 's');
        },
        offset: '100%'
      });
    }

    $('.b-course-info.js-animate').each(function(i){
      var effect = i % 2 ? effects[2] : effects[0];
      animate($(this), effect);
    })

    animate($('.b-author__left.js-animate'), 'fadeInLeft');
    animate($('.b-author__right.js-animate'), 'fadeInRight');

    animate($('.b-certificate__img.js-animate'), 'fadeInUp');
    animate($('.b-certificate__content.js-animate'), 'fadeInRight');

    if ($('.b-course-kit__content.js-animate').length) {
    //partners access
      new Waypoint({
        element: $('.b-course-kit__content.js-animate')[0],
        handler: function() {
          var $this = $(this.element);
          $this.removeClass('js-animate')
          $this.find('.b-course-kit__img').addClass('animated zoomIn').css('animation-delay', startDelay + 's');
          $this.find('.b-course-kit__info').children().each(function(i){
            $(this).addClass('animated fadeInRight').css('animation-delay', startDelay + i * 0.2 + 's');
          })
        },
        offset: '100%'
      });
    }

    $('.b-review.js-animate').each(function(){
      animate($(this), 'fadeInRight');
    })

    if ($('.b-reviews-total.js-animate').length) {
    //partners access
      new Waypoint({
        element: $('.b-reviews-total.js-animate')[0],
        handler: function() {
          var $this = $(this.element);
          $this.removeClass('js-animate');
          $this.find('.b-reviews-total__summary').addClass('animated fadeInLeft').css('animation-delay', startDelay + 's');
          $this.find('.b-reviews-total__row').each(function(i){
            $(this).addClass('animated fadeInUp').css('animation-delay', startDelay + i * 0.2 + 's');
          })
        },
        offset: '100%'
      });
    }

    $('.b-featured .l-col-33.js-animate').each(function(i){
      animate($(this), effects[i % 3]);
    })

    animate($('.b-customers__list.js-animate'), 'zoomIn');

    setTimeout(function(){
      startDelay = 0.1;
    }, startDelay * 1000)


    //this hack for google fetch bot, disable waypoints before user start scrolling, cuz fetch bot can't scroll and doesn't see animated content
    var enabled = true;
    setTimeout(function(){
      Waypoint.disableAll();
      $('.js-animate').css('visibility', 'visible');
      enabled = false;
    }, 100);

    $(window).on('scroll', function(){
      if (!enabled) {
        Waypoint.enableAll();
        $('.js-animate').css('visibility', '');
        enabled = true;
      }
    });

  }
})
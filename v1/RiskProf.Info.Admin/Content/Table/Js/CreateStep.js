﻿if (typeof Kiout == 'undefined') Kiout = {}
if (typeof Kiout.Teu == 'undefined') Kiout.Teu = {}
if (typeof Kiout.Teu.Study == 'undefined') Kiout.Teu.Study = {}
if (typeof Kiout.Teu.Study.Plan == 'undefined') Kiout.Teu.Study.Plan = {}

Kiout.Teu.Study.Plan.CreateStep = function (baseUrl,changeEmployeeUrl, courseOnlineUrl, courseLocalUrl, orderItemsUrl,  employees, token, hardwareId, tableId)
{
    this.BaseUrl = baseUrl;
    this.CourseOnlineUrl = courseOnlineUrl;
    this.CourseLocalUrl = courseLocalUrl;
    this.OrderItemsUrl = orderItemsUrl;
    this.Token = token;
    this.HardwareId = hardwareId;
    this.Employees = employees;
    this.ChangeEmployeeUrl = changeEmployeeUrl;
    this.TableId = tableId;
}

Kiout.Teu.Study.Plan.CreateStep.prototype.SetSelectedEmployeesUrl = function(url)
{
    this.SelectedEmployeesUrl = url;
}

Kiout.Teu.Study.Plan.CreateStep.prototype.ShowFormOnlineCourses = function(done)
{
    var thisObject = this;
    $.post(this.CourseOnlineUrl, { instanceTokenGuid: thisObject.Token, HardwareId: thisObject.HardwareId, ProductTypeId:1 }, function (data) {
        if (data.IsSuccess) {
            $.fancybox(
                {
                    content: data.Data,
                    afterClose: function () {
                        thisObject.GetOrderItems();
                    }
                }
            )

        } else {
            alert("Ошибка: " + data.State + ' ' + data.Message);
        }
     })
          .done(function () {
              if(done!=undefined)
                done();
          })
          .fail(function (xhr, status, error) {
              alert("Возникла ошибка при запросе курсов");
          })
          .always(function () {
          });
}

Kiout.Teu.Study.Plan.CreateStep.prototype.ShowFormLocalCourses = function (done) {
    var thisObject = this;
    $.post(this.CourseLocalUrl, {  }, function (data) {
        if (data.IsSuccess) {
            $.fancybox(
                {
                    content: data.Data,
                    afterClose: function () {
                        thisObject.ClearSelectOrderItems();
                    }
                }
            )

        } else {
            alert("Ошибка: " + data.State + ' ' + data.Message);
        }
    })
          .done(function () {
              if (done != undefined)
                  done();
          })
          .fail(function (xhr, status, error) {
              alert(xhr.responseText);
          })
          .always(function () {
          });
}


Kiout.Teu.Study.Plan.CreateStep.prototype.ChangeCourse = function(courseGuid)
{
    this.CourseGuid = courseGuid;
}

Kiout.Teu.Study.Plan.CreateStep.prototype.GetOrderItems = function(done)
{
    var thisObject = this;
    
    if (thisObject.CourseGuid) {
        $.post(this.OrderItemsUrl, { messageId: thisObject.Token, instanceTokenGuid: thisObject.Token, hardwareId: thisObject.HardwareId, catalogItemGuid: thisObject.CourseGuid }, function (data) {
            if (data.IsSuccess) {
                thisObject.OrderItems = data.Data;
                thisObject.FillSelectOrderItems(thisObject.OrderItems);
                thisObject.SetChangeSelectOrderItems(thisObject.OrderItems);
            } else {
                if (data.State != 404) {
                    alert("Ошибка: " + data.State + ' ' + data.Message);
                }
            }
        })
     .done(function () {
         if (done != undefined)
             done();
     })
     .fail(function (xhr, status, error) {
         alert(xhr.responseText);

     })
     .always(function () {
         //alert("finished");
     });
    }
}

Kiout.Teu.Study.Plan.CreateStep.prototype.ClearSelectOrderItems = function()
{
    $(".select-orderItem").find('.option-order').remove();
}

Kiout.Teu.Study.Plan.CreateStep.prototype.FillSelectOrderItems = function(orderItems)
{
    var thisObject = this;
    if (orderItems == undefined)
        orderItems = this.OrderItems;

    thisObject.ClearSelectOrderItems();
    $.each(orderItems, function (index, element) {
        $.each($(".select-orderItem"), function (index, select) {
            $option = $('<option class="option-order" value="' + element.Guid + '">' + element.Title + '</option>');
            $option.change(function () {
                thisObject.ChangeOptionOrderItem($option);
            });
            $(select).append($option)
        });
    });
}

Kiout.Teu.Study.Plan.CreateStep.prototype.SetChangeSelectOrderItems = function(orderItems)
{
    if (orderItems == undefined)
        orderItems = this.OrderItems;

    $(".select-orderItem").find('option').removeProp("selected");
    

    $.each(orderItems,function(index,element)
    {
        var pos = 1;
        var options = $(".select-orderItem").find('option[value="' + element.Guid + '"]')
        $.each(options,function(index,option)
        {
            $select = $('#' + $(option).parents('.select-orderItem').attr('Id'));
            if (element.CountUsed + pos <= element.Count && !$select.val())
            {
                $(option).prop("selected", true);
                pos++;
            }
        });
    })
}

Kiout.Teu.Study.Plan.CreateStep.prototype.ChangeOptionOrderItem = function (element) {
    var currentOrderItem = null;
    var guid = $(element).val();

    $.each(this.OrderItems, function (index, orderItem) {
        if (orderItem.Guid == guid) {
            currentOrderItem = orderItem;
        }
    })

    var pos = 2;
    var options = $(".select-orderItem").find('.option-order [value="' + currentOrderItem.Guid + '"]')

    $.each(options, function (index, option) {
        if (option != element &&  currentOrderItem.CountUsed + pos < element.Count) {
            option.change();
        }
        pos++;
    });
}

Kiout.Teu.Study.Plan.CreateStep.prototype.GetEmployeeOrderItems = function()
{
    var result = new Array();

    $.each($(".select-orderItem"),function(index,select)
    {
        var $select = $(select);
        result.push({ EmployeeId : $select.attr('data-id'), OrderItemGuid : $select.val() });
    });

    return result;
}


Kiout.Teu.Study.Plan.CreateStep.prototype.SubmitForm = function()
{
    $.blockUI({ css: { backgroundColor: '#FFF', color: '#000' }, message: '<h1><img src="/Content/Images/busy.gif" /> Подождите</h1>' });
    $('.btn').attr('disabled', 'disabled');
    var thisObject = this;
    data = {
        CourseGuid: thisObject.CourseGuid,
        DateStart : $('#DateStart').val(),
        DateEnd : $('#DateEnd').val(),
        PlanReasonId: $('#PlanReasonId').val(),
        ProgramId: $('#ProgramId').val(),
        Employees: JSON.stringify(thisObject.GetEmployeeOrderItems()),
        returnUrl: $('#ReturnUrl').val(),
    }
    $('.field-validation-valid').html('');    
    $.post(this.BaseUrl, data, function (data) {
       
        if (data.IsSuccess) {
            document.location.href = data.Data;
        } else {
            
            alert(data.Message);
            $.each(data.Data, function (index, error) {
                $('span[data-valmsg-for="' + error.ElementId + '"]').html(error.Message);
            });
            $('.btn').removeAttr("disabled");
        }
    })
    .done(function () {
    })
    .fail(function (xhr, status, error) {
        alert("Выберите курс");
        $('.btn').removeAttr("disabled");
    })
    .always(function () {
        $.unblockUI();
    });
}

Kiout.Teu.Study.Plan.CreateStep.prototype.ChangeEmployee = function ()
{
    var thisObject = this;
        $.fancybox(
        {
            type: 'iframe',
            href: this.ChangeEmployeeUrl,
            width       : '90%',
            height      : '90%',
            //afterClose: function () {
            //    thisObject.ClearSelectOrderItems();
            //}
        })
}

Kiout.Teu.Study.Plan.CreateStep.prototype.AddEmployeeIds = function (ids)
{
    var tbody = $('#' + this.TableId).find('tbody');
    $.post(this.SelectedEmployeesUrl, { ids: ids.join(',') }, function (dataJson) {
        var data = JSON.parse(dataJson);
        $.each(data, function (index, item) {
            if (tbody.find('#tr_' + item.EmployeeId).length == 0) {
                var tr = $('<tr></tr>');
                tr.attr('id', 'tr_' + item.EmployeeId)
                var td = $('<td></td>');
                td.addClass('rt-table-col-id')
                td.html(item.EmployeeId);
                tr.append(td);
                var td = $('<td></td>');
                td.addClass('rt-table-col-id');
                td.html(item.FullName);
                tr.append(td);

                var td = $('<td></td>');
                td.addClass('rt-table-col-actions');
                var button = $('<input type="button" value="Удалить из списка" class="btn btn-primary" />');
                button.attr('onclick', '$("#tr_' + item.EmployeeId + '").remove();if($("#employeeTable td").length == 0){$("#employeeTable").attr("hidden", "hidden")}');
                td.append(button);
                tr.append(td);

                var td = $('<td></td>');
                td.addClass('rt-table-col-actions');
                var select = $('<select id="select_' + item.EmployeeId + '" name="select_' + item.EmployeeId + '" data-id="' + item.EmployeeId + '" class="select-orderItem form-control"></select>');
                select.append('<option class="option-buy" value="">Купить</option>');
                td.append(select);
                tr.append(td);
                tbody.append(tr);
            }
        });
        $('#employeeTable').removeAttr("hidden")
    })
     .fail(function (xhr, status, error) {
         alert("Выберите курс");
     })
     .always(function () {
         $.unblockUI();
     });
}
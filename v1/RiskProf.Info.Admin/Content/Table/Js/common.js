'use strict';

var startDelay = 0.3;
function animate(container, animation, delay) {
  if (typeof(delay) === 'undefined') delay = 0;
  var el;
  if (typeof(container) === 'object') {
    el = container;
  } else {
    el = document.querySelectorAll(container);
  }
  if (!el) return undefined;
  for (var i = 0; i < el.length; i++) {
    new Waypoint({
      element: el[i],
      handler: function(direction) {
        delay = delay + startDelay;
        this.element.style.animationDelay = delay + 's';
        this.element.classList.add('animated', animation);
      },
      offset: '90%'
    })
  }
}

$(document).ready(function(){

  //use it to disable animation on mobile devices
  var animationEnabled = true;

  if (animationEnabled) {

    //block intro
    if ($('.b-intro__content').length) {
      new Waypoint({
        element: $('.b-intro__content')[0],
        handler: function() {
          var $this = $(this.element);
          $this.addClass('animated');
          $this.find('.b-intro__title').addClass('animated fadeInDown').css('animation-delay', startDelay + 's');
          $this.find('> p').addClass('animated fadeInUp').css('animation-delay', startDelay + 0.5 + 's');
          $this.find('.b-searchBar').addClass('animated fadeInLeft').css('animation-delay', startDelay + 0.8 + 's');
          $this.find('.b-intro__more').addClass('animated fadeInRight').css('animation-delay', startDelay + 0.8 + 's');
        },
        offset: '100%'
      });
    }

    if ($('.b-access').length) {
      //block access
      new Waypoint({
        element: $('.b-access')[0],
        handler: function() {
          var $this = $(this.element);
          $this.addClass('animated');
          $this.find('.b-title').addClass('animated fadeInLeft').css('animation-delay', startDelay + 's');
          $this.find('.b-access__content > p').addClass('animated fadeInLeft').css('animation-delay', startDelay + 0.2 + 's');
          $this.find('.b-access__content > ul').addClass('animated fadeInLeft').css('animation-delay', startDelay + 0.4 + 's');
          $this.find('.b-digitalShop').addClass('animated fadeInUp').css('animation-delay', startDelay + 0.6 + 's');

          $this.find('.item-pc').addClass('animated fadeInDown').css('animation-delay', startDelay + 0.4 + 's');
          $this.find('.item-laptop').addClass('animated fadeInUp').css('animation-delay', startDelay + 0.6 + 's');
          $this.find('.item-tablet').addClass('animated fadeInRight').css('animation-delay', startDelay + 0.8 + 's');
        },
        offset: '100%'
      });
    }

    if ($('.b-partners').length) {
    //partners access
      new Waypoint({
        element: $('.b-partners')[0],
        handler: function() {
          var $this = $(this.element);
          $this.addClass('animated');
          $this.find('.b-title').addClass('animated fadeInLeft').css('animation-delay', startDelay + 's');
          $this.find('.b-partners__content > p').addClass('animated fadeInLeft').css('animation-delay', startDelay + 0.2 + 's');
          $this.find('.b-partners__buttons > .b-btn--white').addClass('animated fadeInLeft').css('animation-delay', startDelay + 0.4 + 's');
          $this.find('.b-partners__link').addClass('animated zoomIn').css('animation-delay', startDelay + 0.4 + 's');
          $this.find('.b-partners__bg').addClass('animated zoomIn').css('animation-delay', startDelay + 0.4 + 's');
        },
        offset: '100%'
      });
    }

    $('.b-title.js-animate').each(function(){
      animate($(this), 'fadeInLeft');
    });

    var effects = [
      'fadeInLeft',
      'zoomIn',
      'fadeInRight'
    ];

    $('.b-adv-item').each(function(i){
      animate($(this), effects[i % 3]);
    });

    $('.b-popular .l-grid').each(function(){
      $(this).find('.l-col-33').each(function(i){
        animate($(this), effects[i % 3]);
      });
      $(this).find('.l-col-50').each(function(i){
        var effect = i % 2 ? effects[2] : effects[0];
        animate($(this), effect);
      });
      $(this).find('.l-col-100').each(function(i){
        animate($(this), 'fadeInUp');
      });
    });

    $('.b-news-item').each(function(i){
      var effect = i % 2 ? effects[2] : effects[0];
      animate($(this), effect);
    });

    if ($('.b-course-content__list').length) {
      new Waypoint({
        element: $('.b-course-content__list')[0],
        handler: function() {
          $(this.element).addClass('animated');
        },
        offset: '100%'
      });
    }

    $('.b-content').each(function(i){
      animate($(this), 'fadeInRight');
    });

    if ($('.b-sCourse').length) {
      new Waypoint({
        element: $('.b-sCourse')[0],
        handler: function() {
          $(this.element).addClass('animated');
        },
        offset: '100%'
      });
    }

    $('.b-sCourse__option').each(function(i){
      var delay = i * 0.35;
      animate($(this), 'fadeInDown', delay);
    })
    animate($('.b-sCourse__title'), 'fadeInLeft');
    animate($('.b-sCourse__descr'), 'fadeInLeft', 0.2);

    animate($('.b-buy'), 'fadeInRight');
    animate($('.b-video'), 'fadeInLeft');

    if ($('.b-sCourse-details__text').length) {
    //partners access
      new Waypoint({
        element: $('.b-sCourse-details__text')[0],
        handler: function() {
          $(this.element).addClass('animated fadeInLeft').css('animation-delay', startDelay + 's');
        },
        offset: '100%'
      });
    }

    $('.b-course-info').each(function(i){
      var effect = i % 2 ? effects[2] : effects[0];
      animate($(this), effect);
    })

    animate($('.b-author__left'), 'fadeInLeft');
    animate($('.b-author__right'), 'fadeInRight');

    animate($('.b-certificate__img'), 'fadeInUp');
    animate($('.b-certificate__content'), 'fadeInRight');

    if ($('.b-course-kit__content').length) {
    //partners access
      new Waypoint({
        element: $('.b-course-kit__content')[0],
        handler: function() {
          var $this = $(this.element);
          $this.addClass('animated').css('animation-delay', startDelay + 's');;
          $this.find('.b-course-kit__img').addClass('animated zoomIn').css('animation-delay', startDelay + 's');
          $this.find('.b-course-kit__info').children().each(function(i){
            $(this).addClass('animated fadeInRight').css('animation-delay', startDelay + i * 0.2 + 's');
          })
        },
        offset: '100%'
      });
    }

    $('.b-review').each(function(){
      animate($(this), 'fadeInRight');
    })

    if ($('.b-reviews-total').length) {
    //partners access
      new Waypoint({
        element: $('.b-reviews-total')[0],
        handler: function() {
          var $this = $(this.element);
          $this.addClass('animated').css('animation-delay', startDelay + 's');;
          $this.find('.b-reviews-total__summary').addClass('animated fadeInLeft').css('animation-delay', startDelay + 's');
          $this.find('.b-reviews-total__row').each(function(i){
            $(this).addClass('animated fadeInUp').css('animation-delay', startDelay + i * 0.2 + 's');
          })
        },
        offset: '100%'
      });
    }

    $('.b-featured .l-col-33').each(function(i){
      animate($(this), effects[i % 3]);
    })

    animate($('.b-customers__list'), 'zoomIn');

    var enabled = true;

    setTimeout(function(){
      Waypoint.disableAll();
      enabled = false;
    }, 100);

    setTimeout(function(){
      startDelay = 0.1;
    }, startDelay * 1000)

    $(window).on('scroll', function(){
      if (!enabled) {
        Waypoint.enableAll();
        enabled = true;
      }
    });

  } else {
    $('.js-animate').css('visibility', 'visible');
  }

  enquire.register('screen and (min-width: 1170px)', {
    match: function() {
      $(".b-buy").sticky({
        topSpacing: 15,
        zIndex: 5
      });
    },
    unmatch: function() {
      $(".b-buy").unstick();
    }
  });

  $('.b-author__full-text').on('click', function(e){
    e.preventDefault();
    var height = $(this).parent().find('.b-author__text')[0].scrollHeight;
    $(this).parent().find('.b-author__text').css('max-height', height + 'px').addClass('is-open');
    $(this).addClass('is-collapsed');
  })

  $('.b-buy__radio input').on('change', function(){
    if ($(this).data('type') === 'lite') {
      $('.b-buy__content-viewport').css('transform', 'translateX(-100%)');
    } else {
      $('.b-buy__content-viewport').css('transform', '');
    }
  });

  $('.b-btn--menu').on('click', function(e){
    e.preventDefault();
    var $this = $(this);
    var $menu = $('.b-menu');
    if($menu.hasClass('is-open')) {
      $this.removeClass('is-open');
      $menu.removeClass('is-open')
      $(document).off('click.menu');
    } else {
      $this.addClass('is-open');
      $menu.addClass('is-open');
      var firstClick = true;
      $(document).on('click.menu', function(e) {
        if (!firstClick && $(e.target).closest('.b-menu').length == 0) {
          $menu.removeClass('is-open');
          $this.removeClass('is-open');
          $(document).off('click.menu');
        }
      firstClick = false;
      });
    }
  });

  $('body').on('click', '.js-video', function(e){
    e.preventDefault();
    var $this = $(this);
    $.magnificPopup.open({
      items: {
        src: $this.attr('href')
      },
      type: 'iframe',
      mainClass: 'mfp-zoom-in',
      removalDelay: 400,
      autoFocusLast: false
    })
  });

  $('body').on('click', '.js-course-popup', function(e){
    e.preventDefault();
    var $this = $(this);
    $.magnificPopup.open({
      items: {
        src: $this.find('.b-content__popup')
      },
      type: 'inline',
      removalDelay: 400,
      mainClass: 'mfp-zoom-in',
      autoFocusLast: false
    })
  });

  (function(){

    $('.b-customer').on('click', function(e){
      e.preventDefault();
      var $this = $(this);
      $this.addClass('is-active').siblings().removeClass('is-active');

      var oldOffset = $('.b-customers__infobox').offset().top;
      var curoffset = $this.offset().top;

      $('.b-customer').each(function() {
        if($(this).offset().top < curoffset + 5 && $(this).offset().top > curoffset - 5){
          var infobox = $('.b-customers__infobox').detach();
          $(this).before(infobox);
          return false;
        }
      });

      $('.b-customers__infobox').empty();
      var offset = $('.b-customers__infobox').offset().top
      $this.find('.b-cContent').clone().appendTo('.b-customers__infobox');

      if (oldOffset != offset) {
       $('html').scrollTop(oldOffset - 100);
      }

      $('html').animate({
        scrollTop: offset - 100
      }, 400);

    })

    $('.b-customers__infobox').empty();
    $('.b-customer').first().addClass('is-active').find('.b-cContent').clone().appendTo('.b-customers__infobox');


  })();

});

$(window).on('load', function(){
  
})
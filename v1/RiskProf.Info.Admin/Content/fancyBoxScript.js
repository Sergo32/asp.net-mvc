$(document).ready(function () {
    $('a.fancybox').fancybox({
        'autoSize': false,
        'width': 1400,
        'height': 800,
        helpers: {
            overlay: { locked: false }
        }
    });
});

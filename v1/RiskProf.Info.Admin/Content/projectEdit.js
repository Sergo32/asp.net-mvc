$("#ShowSettings").click(function () {
    var val = $('#additionalSettings').css("display");
    if (val == "block") {
        $('#additionalSettings').css("display", "none");
    }
    if (val == "none") {
        $('#additionalSettings').css("display", "block");
    }
});

function changeOrganizationName(organization) {
    $("#OrgTitle").val(organization.value);
}

function changeOrganization(select) {
    var eid = select.value;
    $.ajax({
        url: '@Url.Action("List", "Risk")',
        data: { 'OrganizationId': eid },
        success: function (data) {

            $('#AdressFact').val(data.Address);
            $('#AdressOfficial').val(data.Address);
            $('#OrganizationTitle').val(data.Title);
            $('#Inn').val(data.Inn);

        },
        error: function (err) {
            alert('������ ����������!');
        }
    });
}

$(function () {
    $('.date').datetimepicker({ language: 'ru', pickTime: false });
});
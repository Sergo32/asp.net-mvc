﻿RiskProf.InfoApiClient = function (infoApiWebAppUrl) {
    this._postInfoItemListUrl = infoApiWebAppUrl + 'ajax/post/infoItemList';
    this._postInfoItemUrl = infoApiWebAppUrl + 'ajax/post/infoItemById';
    this._postСategoryUrl = infoApiWebAppUrl + 'ajax/post/categories';
}

RiskProf.InfoApiClient.prototype.GetItemsList = function (model, header, handler) {
    var thisObject = this;
    $.ajax({
        type: "Post",
        url: thisObject._postInfoItemListUrl,
        data: model,
        headers: { 'RiskProf_UserToken': header },
        success: function (data) {
            handler(data);
        },
        error: function (jqXHR, exception) {
            alert(jqXHR.statusText);
        },
    });
}

RiskProf.InfoApiClient.prototype.GetItemById = function (model, header, handler) {
    var thisObject = this;
    $.ajax({
        type: "Post",
        url: thisObject._postInfoItemUrl,
        data: model,
        headers: { 'RiskProf_UserToken': header },
        success: function (data) {
            handler(data);
        },
        error: function (jqXHR, exception) {
            alert(jqXHR.statusText);
        },
    });
}


RiskProf.InfoApiClient.prototype.GetСategoryListByPageGuid = function (model, header, handler) {
    var thisObject = this;
    $.ajax({
        type: "Post",
        url: thisObject._postСategoryUrl,
        data: model,
        headers: { 'RiskProf_UserToken': header },
        success: function (data) {
            handler(data);
        },
        error: function (jqXHR, exception) {
            alert(jqXHR.statusText);
        },
    });
}

RiskProf.InfoApiClient.Instance = new RiskProf.InfoApiClient(RiskProf.Settings.InfoApiWebAppUrl);
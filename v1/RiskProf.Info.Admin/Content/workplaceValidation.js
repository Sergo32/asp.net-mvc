﻿function WorkplaceValidation(dialog) {
	console.log('WorkplaceValidation');
	$(document).on('click', '.tdHazardSource > input', function () {
		if ($(this).is(':checked'))
			$(this).closest('tr').find('.tableWorkType').removeClass('disabled').find('input').prop('disabled', false);
		else
			$(this).closest('tr').find('.tableWorkType').addClass('disabled').find('input').prop('disabled', true);		
		if ($(this).closest('tbody').find('.checkHazardSource:checkbox:checked').length === 0)
		{
			BootstrapDialog.show({
				title: 'Внимание!',
				message: 'Хотя бы один <strong>источник опасности</strong> должен быть выбран.',
				closable: false,
				size: 'size-small',
				type: 'type-info',
				buttons: [{
					label: 'Ок',
					cssClass: 'btn-outline-info',
					action: function (dialog) {
						dialog.close();
					}
				}]
			});
			$(this).prop('checked', true);
		}
	});

	$(document).on('click', '.tdHazard > input', function () {
		if ($(this).is(':checked'))
			$(this).closest('tr').find('.tableHazardSource').removeClass('disabled').find('input').prop('disabled', false);
		else
			$(this).closest('tr').find('.tableHazardSource').addClass('disabled').find('input').prop('disabled', true);	
		if ($('.checkHazard:checkbox:checked').length === 0)
			dialog.getButton('btnSave').disable();
		else dialog.getButton('btnSave').enable(false);
	});
}
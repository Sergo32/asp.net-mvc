﻿
RiskProf.HazardClient = function (factorsServiceUrl) {
    this._getHazardUrl = factorsServiceUrl + 'hazard/get';
    this._getHazardListUrl = factorsServiceUrl + 'hazard/getlist';
}

RiskProf.HazardClient.prototype.Get = function (model, handler) {
    var thisObject = this;
    $.ajax({
        type: "GET",
        url: thisObject._getHazardUrl,
        data: model,
        success: function (data) {
            handler(data);
        },
        error: function (jqXHR, exception) {
            alert(jqXHR.statusText);
        }
        
    });
}

RiskProf.HazardClient.prototype.GetList = function (model, handler) {
    var thisObject = this;
    $.ajax({
        type: "GET",
        url: thisObject._getHazardListUrl,
        data: model,
        success: function (data) {
            handler(data);
        },
        error: function (jqXHR, exception) {
            alert(jqXHR.statusText);
        },
    });
}

RiskProf.HazardClient.Instance = new RiskProf.HazardClient(RiskProf.Settings.hazardUrl);

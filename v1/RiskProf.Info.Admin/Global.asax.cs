﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Autofac;
using Autofac.Integration.Mvc;
using RiskProf.Helpers;
using RiskProf.Helpers.Auth;
using RiskProf.Info.Admin.Configuration;
using RiskProf.Auth.Client;

namespace RiskProf.Info.Admin
{
    public class Global : System.Web.HttpApplication
    {
        protected void ApplicationDependencyConfig()
        {


            var infoConfig = new ConfigurationHelper("Test.Info.Admin");
            string infoConnectionString = infoConfig.GetConnectionString("Test");

            // зависимости
            var builder = new ContainerBuilder();
            {
                builder.Register(x => DependencyResolver.Current).As<IDependencyResolver>().SingleInstance();
                builder.Register(x => (IContainer)((AutofacDependencyResolver)DependencyResolver.Current).ApplicationContainer).As<IContainer>().SingleInstance();

            
                builder.RegisterModule(new RiskProfInfoAPIAutofacModule
                {
                    ConnectionString = infoConnectionString,
                    AdminViews = new AdminViews
                    {
                        Edit = "~/Views/InfoAdmin/Edit.cshtml",
                        AdminPanel = "~/Views/InfoAdmin/AdminPanel.cshtml",
                        Header = "~/Views/Partials/_Header.cshtml",
                        NavMenu = "~/Views/Partials/_navMenu.cshtml",
                        NoAccess = "~/Views/Shared/NoAccess.cshtml"
                    }
                });
            }
            IContainer container = builder.Build();
            {
                DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            }
        }
        protected void Application_Start(object sender, EventArgs e)
        {
            this.ApplicationDependencyConfig();

            RouteConfig.Configure(RouteTable.Routes);
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}
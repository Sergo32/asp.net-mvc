﻿namespace RiskProf.Info.Admin.Configuration
{
    using Autofac;
    using RiskProf.Info.Admin.Web.Controllers;
    using RiskProf.Info.Repository;

    public class RiskProfInfoAPIAutofacModule: Module
    {
        public string ConnectionString { get; set; }
        public AdminViews AdminViews { get; set; }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<InfoItemRepository>()
                    .As<IInfoItemRepository>()
                    .WithParameter("connectionString", this.ConnectionString)
                    .SingleInstance();

            builder.RegisterType<InfoAdminController>()
                   .WithParameter("adminPanelView", AdminViews.AdminPanel)
                   .WithParameter("editView", AdminViews.Edit)
                   .WithParameter("noAccessView", AdminViews.NoAccess)
                    .WithParameter("headerView", AdminViews.Header)
                   .WithParameter("navMenuView", AdminViews.NavMenu);
        }
    }
}
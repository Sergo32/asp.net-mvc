﻿
namespace RiskProf.Info.Admin.Configuration
{
    public class AdminViews
    {
        public string AdminPanel { get; set; }
        public string Edit { get; set; }
        public string Header { get; set; }
        public string NavMenu { get; set; }
        public string NoAccess { get; set; }



    }
}
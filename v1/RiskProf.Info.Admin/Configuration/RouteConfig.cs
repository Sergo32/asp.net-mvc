﻿namespace RiskProf.Info.Admin.Configuration
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;
    using RiskProf.Auth.Client;
    using RiskProf.Helpers.Routes;
    using RiskProf.Info.Admin.Web.Controllers;
    using RiskProf.Info.Dto;
    using RiskProf.Info.Filter;

    public class RouteConfig
    {
        internal static void Configure(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.AddController<InfoAdminController>()
                .ActionAction("", x => x.InfoItemAdminPanel)
                .ActionAction<int?>("edit", x => x.EditInfoItem)
                .ActionAction("getCategory", x => x.GetCategoryList)
                .ActionAction<InfoItemFilter>("getPanel", x => x.GetInfoItemAdminPanel)
                .ActionAction<int?, int>("activeItem", x => x.InfoItemActive)
                .ActionAction<int?>("deleteItem", x => x.DeleteInfoItem)
                .ActionAction("header", x => x.Header)
                .ActionAction("navMenu", x => x.NavMenu)
                .NeedHttpGet()
                .ActionAction<InfoItemJSON>("save", x => x.SaveInfoItem).NeedHttpPost();

        


        }
    }
}